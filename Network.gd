extends Node

# Connection variables

var NetworkENet : ENetMultiplayerPeer

# Connection functions

func connect_to_server() -> void:
	print("Attempting to connect to server")
	NetworkENet = ENetMultiplayerPeer.new()
	NetworkENet.create_client(Settings.ServerIp, Settings.ServerPort)
	multiplayer.multiplayer_peer = NetworkENet
	multiplayer.connect("connected_to_server", connection_succeeded)
	multiplayer.connect("connection_failed", connection_failed)

func connection_succeeded() -> void:
	print("Connection successful")
	get_node("/root/Menu").connection_succeeded()

func connection_failed() -> void:
	print("Connection failed")
	get_node("/root/Menu").connection_failed()

func close_server_connection() -> void:
	print("Closing connection to server")
	get_node("/root/Menu").connection_failed()
	NetworkENet.close()

# Main menu functions

@rpc("authority")
func set_joined_games(games : Dictionary) -> void:
	# Show joined games menu panel
	get_node("/root/Menu/%JoinedGamesMenuPanel").set_games(games)

@rpc("authority")
func set_public_games(games : Dictionary) -> void:
	# Show public games menu panel
	get_node("/root/Menu/%PublicGamesMenuPanel").set_games(games)

@rpc("authority")
func set_game_info(gameinfo : Dictionary) -> void:
	# Show game info menu panel
	get_node("/root/Menu/%GameInfoMenuPanel").set_game_info(gameinfo)

@rpc("authority")
func user_approved() -> void:
	print("User approved")

@rpc("authority")
func user_rejected() -> void:
	print("User rejected")

@rpc("authority")
func change_to_game_scene() -> void:
	get_tree().change_scene_to_file("res://Scenes/Game/Game.tscn")

# Variable functions

@rpc("authority")
func set_game_variable(variable : String, value) -> void:
	if GameVariables.get(variable) != null:
		GameVariables.set(variable, value)
		match variable:
			"StartTime":
				if get_tree().current_scene.name == "Game":
					if value != -1:
						get_node("/root/Game").unpause_game()
			"Users":
				if get_tree().current_scene.name == "Game":
					for ordernode in get_node("/root/Game/Orders/Players/0").get_children():
						ordernode.set_user()
					get_node("/root/Game/%LeaderboardPanel").update_tick()
					get_node("/root/Game").update_game_start_label()
			"OnlineUsers":
				if get_tree().current_scene.name == "Game":
					get_node("/root/Game").update_online_players()
			"GenerateTroopsTicksDivisor":
				GameVariables.GenerateTroopsTicks = GameVariables.DefaultGenerateTroopsTicks / GameVariables.GenerateTroopsTicksDivisor
			"GenerateShieldMaximumTicksDivisor":
				GameVariables.GenerateShieldMaximumTicks = GameVariables.DefaultGenerateShieldMaximumTicks / GameVariables.GenerateShieldMaximumTicksDivisor
			"GenerateOreTicksDivisor":
				GameVariables.GenerateOreTicks = GameVariables.DefaultGenerateOreTicks / GameVariables.GenerateOreTicksDivisor
	else:
		# Failure case
		print("\"" + variable + "\" is not a valid game variable")

# Order functions

@rpc("authority")
func set_order(order : Dictionary) -> void:
	var OrdersNode : Node = get_node("/root/Game").get_orders_node(order)
	OrdersNode.create_order(order)

@rpc("authority")
func remove_order(ordertype : String, orderid : int) -> void:
	get_node("/root/Game/Orders/" + ordertype + "/" + str(orderid)).remove_order()

@rpc("authority")
func set_revealed_orders(revealedorders : Array) -> void:
	for order in revealedorders:
		set_order(order)

@rpc("authority")
func set_create_submarine_order_initial_troops(ordertick : int, orderid : int, troops : int) -> void:
	get_node("/root/Game/Orders/Submarines/" + str(ordertick) + "/" + str(orderid)).set_initial_troops(troops)

# Communication functions

@rpc("authority")
func set_message(users : Array, message : Dictionary) -> void:
	if GameVariables.Chats.has(users) == false:
		GameVariables.Chats[users] = []
	GameVariables.Chats[users].append(message)
	get_node("/root/Game/%CommunicationsPanel").update_messages(users)

# Server order functions

@rpc("any_peer", "call_remote")
func set_server_order(gameid : int, id : int, order : Dictionary) -> void:
	return

@rpc("any_peer", "call_remote")
func remove_server_order(gameid : int, id : int, ordertick : int, orderid : int) -> void:
	return

@rpc("any_peer", "call_remote")
func set_server_create_submarine_order_initial_troops(gameid : int, id : int, ordertick : int, orderid : int, troops : int) -> void:
	return

@rpc("any_peer", "call_remote")
func get_revealed_orders(gameid : int, id : int, user : String, tick : int) -> void:
	return

# Server game functions

@rpc("any_peer", "call_remote")
func create_game(id : int, user : String, gameinfo : Dictionary) -> void:
	return

@rpc("any_peer", "call_remote")
func open_game(gameid : int, id : int, user : String) -> void:
	return

@rpc("any_peer", "call_remote")
func close_game(gameid : int, id : int, user : String) -> void:
	return

@rpc("any_peer", "call_remote")
func get_game_info(gameid : int, id : int) -> void:
	return

@rpc("any_peer", "call_remote")
func get_joined_games(id : int, user : String) -> void:
	return

@rpc("any_peer", "call_remote")
func get_public_games(id : int, user : String) -> void:
	return

@rpc("any_peer", "call_remote")
func join_game(gameid : int, id : int, user : String) -> void:
	return

# Server communication functions

@rpc("any_peer", "call_remote")
func send_message(gameid : int, id : int, users : Array, message : Dictionary) -> void:
	return
