extends Button

@export var TogglePanel : String

func _on_BottomBarButton_toggled(buttonpressed : bool) -> void:
	if TogglePanel != "":
		if buttonpressed:
			# Toggle all other buttons
			for buttonnode in get_node("/root/Game/Interface/BottomBar/HBoxContainer").get_children():
				buttonnode.button_pressed = buttonnode == self
			# Show toggle panel
			get_node("/root/Game/%" + TogglePanel).show()
		else:
			# Hide toggle panel
			get_node("/root/Game/%" + TogglePanel).hide()

func _on_BottomBarButton_pressed():
	if button_pressed == false:
		get_node("/root/Game").set_visible_tick(GameVariables.GlobalTick)
