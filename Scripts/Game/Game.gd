extends Node2D

var CalculateGameStateTick : int

# Inbuilt functions

func _ready() -> void:
	seed(GameVariables.GameId)
	calculate_game_state()

# Game state functions

# Calculates the current game state from game start
func calculate_game_state() -> void:
	# Set game variables
	GameVariables.AllActive = true
	# Unpause game
	if GameVariables.StartTime != -1:
		unpause_game()
	# Set initial orders
	var SetInitialOrdersStartTime : int = Time.get_ticks_msec()
	for child in $Orders.get_children():
		child.set_initial_orders()
	print("Setting initial orders took " + str(Time.get_ticks_msec() - SetInitialOrdersStartTime) + "ms")
	# Calculate game state expansion
	var CalculateGameStateExpansionStartTime : int = Time.get_ticks_msec()
	calculate_game_state_expansion(0)
	print("Calculating game state expansion took " + str(Time.get_ticks_msec() - CalculateGameStateExpansionStartTime) + "ms")

# Calculates the current game state at tick
func calculate_game_state_expansion(tick : int) -> void:
	print("Started calculating game state expansion at " + str(tick))
	# Set maximum tick
	var PreviousMaximumTick : int = GameVariables.MaximumTick
	if GameVariables.GameEndTick != -1:
		GameVariables.MaximumTick = min(
			GameVariables.GlobalTick + Settings.MaximumTickAhead,
			GameVariables.MaximumTick + Settings.MaximumTickAhead
		)
	else:
		GameVariables.MaximumTick = GameVariables.GlobalTick + Settings.MaximumTickAhead
	# Calculate game state
	var CalculateGameStateTick : int = tick
	while CalculateGameStateTick <= GameVariables.MaximumTick:
		# Check if exceeding previous game state maximum
		if CalculateGameStateTick > PreviousMaximumTick:
			# Set items active
			if GameVariables.AllActive == false:
				GameVariables.AllActive = true
				for itemsnode in $Items.get_children():
					for itemnode in itemsnode.get_children():
						itemnode.set_active(CalculateGameStateTick)
		# Calculate active items history expansion
		for itemsnode in $Items.get_children():
			itemsnode.calculate_active_items_history_expansion(CalculateGameStateTick)
		# Set user orders
		for orderid in GameVariables.Orders.get(CalculateGameStateTick, []):
			if $Orders.has_node(str(orderid)) == false:
				var Order : Dictionary = GameVariables.Orders[CalculateGameStateTick][orderid]
				var OrdersNode : Node = get_orders_node(Order)
				OrdersNode.create_order(Order)
		# Calculate active items history effect
		var ReversedItemsNodes : Array = $Items.get_children()
		ReversedItemsNodes.reverse()
		for itemsnode in ReversedItemsNodes:
			itemsnode.calculate_active_items_history_effect(CalculateGameStateTick)
		# Update outpost sonar
		for outpostnode in $Items/Outposts.get_children():
			if CalculateGameStateTick == outpostnode.CreateItemOrder["OrderTick"]:
				if outpostnode.Active:
					outpostnode.update_sonar(CalculateGameStateTick)
		# Check for game end
		calculate_game_end(CalculateGameStateTick)
		CalculateGameStateTick += 1
	# Set items active false
	for itemsnode in $Items.get_children():
			itemsnode.set_items_active_false(CalculateGameStateTick)
	# Update tick
	if GameVariables.AllActive == false:
		if tick <= GameVariables.VisibleTick:
			get_node("/root/Game").update_tick()
	# Reset active variables
	GameVariables.AllActive = false
	GameVariables.Orders = {}

# Game end functions

func calculate_game_end(tick : int) -> void:
	if GameVariables.GameEndTick == -1 or tick <= GameVariables.GameEndTick:
		GameVariables.GameEndTick = -1
		match GameVariables.VictoryType:
			"Domination":
				calculate_domination_game_end(tick)
			"Mining":
				calculate_mining_game_end(tick)
		if GameVariables.GameEndTick != -1:
			# Set maximum tick
			GameVariables.MaximumTick = min(
				GameVariables.GlobalTick + Settings.MaximumTickAhead,
				GameVariables.MaximumTick + Settings.MaximumTickAhead
			)
			# Set visible tick
			get_node("/root/Game").set_visible_tick(GameVariables.VisibleTick)

func calculate_domination_game_end(tick : int) -> void:
	for playernode in get_node("/root/Game/Items/Players").get_children():
		var PlayerHistoryEntry : Dictionary = playernode.get_history_entry(tick)
		var User : String = playernode.CreateItemOrder["User"]
		if PlayerHistoryEntry["OutpostsTotal"] >= GameVariables.DominationVictoryTotal:
			set_game_end(tick, User)
			# Check stalemate
			if GameVariables.GameEndUser:
				return

func calculate_mining_game_end(tick : int) -> void:
	for playernode in get_node("/root/Game/Items/Players").get_children():
		var PlayerHistoryEntry : Dictionary = playernode.get_history_entry(tick)
		var User : String = playernode.CreateItemOrder["User"]
		if PlayerHistoryEntry["OreTotal"] >= GameVariables.MiningVictoryTotal:
			set_game_end(tick, User)
			# Check stalemate
			if GameVariables.GameEndUser:
				return

func set_game_end(tick : int, user : String) -> void:
	if GameVariables.GameEndTick == -1:
		# Set game end
		GameVariables.GameEndTick = tick
		GameVariables.GameEndUser = user
		GameVariables.GlobalTick = min(GameVariables.GlobalTick, GameVariables.GameEndTick)
	else:
		# Set stalemate
		GameVariables.GameEndTick = -1
		GameVariables.GameEndUser = ""

# Position functions

# Get position wrapped over map
func get_wrapped_position(wrapposition : Vector2, anchorposition : Vector2 = $Camera2D.global_position) -> Vector2:
	var GridPosition : Vector2 = (anchorposition - wrapposition) / GameVariables.MapSize + Vector2(0.5, 0.5)
	return Vector2(floor(GridPosition.x), floor(GridPosition.y)) * GameVariables.MapSize + wrapposition

# Get position wrapped over map closest to anchor position
func get_closest_wrapped_position(wrapposition : Vector2, anchorposition : Vector2) -> Vector2:
	var WrappedPositionDifference : Vector2 = Vector2(wrapposition.x - anchorposition.x, wrapposition.y - anchorposition.y)
	var GridOffset : Vector2 = Vector2(
		GameVariables.MapSize.x * (int(WrappedPositionDifference.x > GameVariables.HalfMapSize.x) - int(WrappedPositionDifference.x < -GameVariables.HalfMapSize.x)),
		GameVariables.MapSize.y * (int(WrappedPositionDifference.y > GameVariables.HalfMapSize.y) - int(WrappedPositionDifference.y < -GameVariables.HalfMapSize.y))
	)
	return anchorposition + GridOffset

# Tick functions

# Set global tick
func set_global_tick(globaltick : int, setvisibletick : bool = false) -> void:
	print("Set global tick to " + str(globaltick))
	# Check that game has not ended
	if GameVariables.GameEndTick == -1 or globaltick <= GameVariables.GameEndTick:
		# Set tick variables
		var PreviousGlobalTick : int = GameVariables.GlobalTick
		GameVariables.GlobalTick = globaltick
		# Update items
		for itemsnode in $Items.get_children():
			for itemnode in itemsnode.get_children():
				var ItemHistoryEntry : Dictionary = itemnode.get_history_entry(globaltick)
				# Check that item is present at tick
				if ItemHistoryEntry.is_empty() == false:
					# Set item active
					if (
						ItemHistoryEntry.get("EffectedOutposts", []).is_empty() == false or
						ItemHistoryEntry.get("EffectedSubmarines", []).is_empty() == false
					):
						itemnode.set_active(globaltick)
		# Calculate history expansion
		calculate_game_state_expansion(GameVariables.GlobalTick)
		# Checks if the ruler starting tick should be increased alongside global tick
		if $Ruler.RulerStartTick == PreviousGlobalTick:
			$Ruler.RulerStartTick = GameVariables.GlobalTick
		# Removes past queued order buttons
		$Interface/Panels/BottomBar/TimePanel.remove_past_orders()
		# Checks if the global tick should be set
		if (
			setvisibletick or
			(GameVariables.VisibleTick + 1 == GameVariables.GlobalTick and GameVariables.Jumping == false)
		):
			GameVariables.VisibleTick = GameVariables.GlobalTick
		# Update tick
		get_node("/root/Game").update_tick()
		# Gets all orders revealed on this tick
		Network.rpc_id(
			1,
			"get_revealed_orders",
			# Game ID
			GameVariables.GameId,
			# Client ID
			multiplayer.get_unique_id(),
			# User
			Settings.User,
			# Global tick
			GameVariables.GlobalTick
		)

# Unpauses the game
func unpause_game() -> void:
	print("Game unpaused")
	var TimeSinceStart : int = Time.get_unix_time_from_system() - GameVariables.StartTime
	# Set tick variables
	GameVariables.GlobalTick = floor(TimeSinceStart / GameVariables.TickLength)
	GameVariables.VisibleTick = GameVariables.GlobalTick
	# Start tick timer
	$TickTimer.start(GameVariables.TickLength - (TimeSinceStart % GameVariables.TickLength))
	# Update tick
	get_node("/root/Game").update_tick()

# Node functions

# Returns node for passed item
func get_item_node(item : String) -> Node:
	# Get outpost node
	if $Items/Outposts.has_node(item):
		return $Items/Outposts.get_node(item)
	# Get submarine node
	if $Items/Submarines.has_node(item):
		return $Items/Submarines.get_node(item)
	# Failure case
	return Node.new()

func get_orders_node(order : Dictionary) -> Node:
	for ordersnode in $Orders.get_children():
		if ordersnode.OrderScenes.has(order["Type"]):
			return ordersnode
	# Failure case
	return Node.new()

func get_user_player(user : String) -> String:
	for createplayerorderticknode in get_node("/root/Game/Orders/Players").get_children():
		for createplayerordernode in createplayerorderticknode.get_children():
			var CreatePlayerOrder : Dictionary = createplayerordernode.Order
			if CreatePlayerOrder["Type"] == "CreatePlayer":
				if CreatePlayerOrder["User"] == user:
					return CreatePlayerOrder["Player"]
	# Failure case
	return ""
