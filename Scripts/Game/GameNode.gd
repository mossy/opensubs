extends "res://Scripts/Game/Game.gd"

@export var DistanceToHoverOutpost : float = 50.0
@export var MapCursorMovementAllowance : float = 10.0

@onready var TickTween : Tween
@onready var MapTween : Tween

var OnlineIndicatorScene : PackedScene = preload("res://Scenes/InterfaceElements/OnlineIndicator.tscn")

var Exiting : bool = false
var CursorPosition : Vector2
var PressedMapCursorPosition : Vector2
var PreviousCameraPosition : Vector2
var HoveredStartOutpost : Node
var HoveredEndOutpost : Node
var MapCursorMovementMaximum : float
var CurrentZoom : float = 1.0
var ZoomMultiplier : float = 2.0

# Inbuilt functions

func _ready() -> void:
	super()
	# Update game state
	update_tick()
	update_online_players()
	update_game_start_label()
	# Get camera starting position
	var PlayerOutpostAnchorPosition : Vector2
	var PlayerOutpostsWrappedPositions : Array
	for outpostnode in $Items/Outposts.get_children():
		var OutpostHistoryEntry : Dictionary = outpostnode.History[0]
		if OutpostHistoryEntry["CurrentPlayer"] == GameVariables.Player:
			var OutpostWrappedPosition : Vector2
			if PlayerOutpostsWrappedPositions.is_empty():
				PlayerOutpostAnchorPosition = OutpostHistoryEntry["Position"]
				OutpostWrappedPosition = OutpostHistoryEntry["Position"]
			else:
				OutpostWrappedPosition = get_closest_wrapped_position(PlayerOutpostAnchorPosition, OutpostHistoryEntry["Position"])
			PlayerOutpostsWrappedPositions.append(OutpostWrappedPosition)
	var CameraPosition : Vector2
	for playeroutpostposition in PlayerOutpostsWrappedPositions:
		CameraPosition += playeroutpostposition
	# Set camera starting position
	CameraPosition = CameraPosition / len(PlayerOutpostsWrappedPositions)
	CameraPosition = get_wrapped_position(CameraPosition)
	set_camera_position(CameraPosition)
	# Connect window resize signal to update camera zoom
	get_tree().get_root().size_changed.connect(update_camera_zoom)

func _process(delta : float) -> void:
	# Update tween values
	if TickTween != null and TickTween.is_running():
		set_visible_tick(int(GameVariables.VisibleTick))
	if MapTween != null and MapTween.is_running():
		update_map()
	# Update map zoon
	if $"%CommunicationsPanel".visible == false:
		if Input.is_action_just_released("ZoomIn") or Input.is_action_just_released("ZoomOut"):
			if Input.is_action_just_released("ZoomIn"):
				CurrentZoom += ZoomMultiplier * delta
			elif Input.is_action_just_released("ZoomOut"):
				CurrentZoom -= ZoomMultiplier * delta
			update_camera_zoom()
	# Update map position
	if (
		$MapControls/MapButton.button_pressed and
		(MapTween == null or MapTween.is_running() == false)
	):
		var CameraPosition : Vector2 = $Camera2D.global_position + PressedMapCursorPosition - get_global_mouse_position()
		set_camera_position(CameraPosition)
		PreviousCameraPosition = CameraPosition
		var MapCursorMovementDifference : float = PressedMapCursorPosition.distance_to(get_global_mouse_position())
		if MapCursorMovementDifference > MapCursorMovementMaximum:
			MapCursorMovementMaximum = MapCursorMovementDifference
	# Update hovered outposts
	HoveredEndOutpost = null
	for child in $Items/Outposts.get_children():
		if child != HoveredStartOutpost:
			var OutpostPosition : Vector2 = child.global_position
			if get_global_mouse_position().distance_to(OutpostPosition) <= DistanceToHoverOutpost:
				HoveredEndOutpost = child
				child.focus()
				CursorPosition = OutpostPosition
			elif child.Focused:
				child.unfocus()
	if HoveredEndOutpost == null:
		CursorPosition = get_global_mouse_position()

# Tick functions

# Set visible tick
func set_visible_tick(tick : int) -> void:
	var PreviousVisibleTick : int = GameVariables.VisibleTick
	var CurrentVisibleTick : int = min(tick, GameVariables.MaximumTick)
	if CurrentVisibleTick != PreviousVisibleTick:
		GameVariables.VisibleTick = CurrentVisibleTick
		update_tick()

# Update tick
func update_tick() -> void:
	# Update items tick
	for itemsnode in $Items.get_children():
		for itemnode in itemsnode.get_children():
			itemnode.update_tick()
	# Update panels tick
	for panelssnode in $Interface/Panels.get_children():
		for panelnode in panelssnode.get_children():
			# TODO: reconsider
			if panelnode.has_method("update_tick"):
				panelnode.update_tick()
	# Update game state labels
	update_game_start_label()
	update_game_end_label()

# Jumps time control to passed tick
func jump_to_tick(tick : int) -> void:
	GameVariables.Jumping = true
	# Start tick tween
	TickTween = get_tree().create_tween()
	TickTween.tween_method(set_visible_tick, GameVariables.VisibleTick, min(tick, GameVariables.MaximumTick - 1), 1)
	TickTween.connect("finished", _on_TickTween_finished)
	# Show time panel
	$"%TimeButton".button_pressed = true

# Jumps time control to submarine arrival and jumps camera position to target position
func jump_to_arrival(arrivalsubmarine : String) -> void:
	# Get arrival submarine data
	var ArrivalSubmarineNode : Node = $Items/Submarines.get_node(arrivalsubmarine)
	var ArrivalSumbarineHistory : Array = ArrivalSubmarineNode.History
	var ArrivalSubmarineHistoryEntry : Dictionary = ArrivalSumbarineHistory[-1]
	var ArrivalTick : int = ArrivalSubmarineHistoryEntry["Tick"]
	# Get target data
	var Target : String = ArrivalSubmarineHistoryEntry["CurrentTarget"]
	var TargetNode : Node = get_item_node(Target)
	var TargetHistoryEntry : Dictionary = TargetNode.get_history_entry(ArrivalTick)
	var TargetPosition : Vector2 = TargetHistoryEntry["Position"]
	var TargetWrappedPosition : Vector2 = get_wrapped_position(TargetPosition)
	# Start map tween
	MapTween = get_tree().create_tween()
	MapTween.tween_property($Camera2D, "global_position", TargetWrappedPosition, 1)
	MapTween.connect("finished", _on_MapTween_finished)
	# Jumps time control to submarine arrival
	jump_to_tick(ArrivalTick)

# Data functions

func set_hovered_start_outpost(outpostnode : Node) -> void:
	HoveredStartOutpost = outpostnode
	if HoveredEndOutpost == outpostnode:
		HoveredEndOutpost = null
	$Ruler.RulerStartTick = GameVariables.VisibleTick

# Visual functions

func set_camera_position(cameraposition : Vector2) -> void:
	if cameraposition != $Camera2D.global_position:
		$Camera2D.global_position = cameraposition
		update_map()

func update_camera_zoom() -> void:
	CurrentZoom = clamp(CurrentZoom, max(get_window().get_size().x, get_window().get_size().y) / min(GameVariables.MapSize.x, GameVariables.MapSize.y), 10)
	$Camera2D.zoom = Vector2(CurrentZoom, CurrentZoom)

func update_map() -> void:
	# Set background size
	$Background.global_position = Vector2(
		snapped($Camera2D.global_position.x, 50),
		snapped($Camera2D.global_position.y, 50)
	) - GameVariables.HalfMapSize - Vector2(100, 100)
	$Background.size = GameVariables.MapSize + Vector2(200, 200)
	$Background/SubViewport.size = $Background.size
	$%Masks.global_position = $Background.global_position * -1
	# Update positions
	for child in $Items/Outposts.get_children():
		child.update_position()
	for child in $Items/Submarines.get_children():
		child.update_position()

func update_game_start_label() -> void:
	$"%GameStartLabel".hide()
	if len(GameVariables.Users) < GameVariables.PlayerCount and GameVariables.VisibleTick == GameVariables.GlobalTick:
		$"%GameStartLabel".show()
		$"%GameStartLabel".text = "Waiting for more players to join (" + str(len(GameVariables.Users)) + "/" + str(GameVariables.PlayerCount) + " present)"

func update_game_end_label() -> void:
	$"%GameEndLabel".hide()
	if GameVariables.GameEndTick != -1 and $"%GameStartLabel".visible == false:
		var TimeToGameEnd : int = (GameVariables.GameEndTick - GameVariables.VisibleTick) * GameVariables.TickLength
		$"%GameEndLabel".show()
		if TimeToGameEnd > 0:
			$"%GameEndLabel".text = GameVariables.GameEndUser + " wins in " + VariableFunctions.get_time_text(TimeToGameEnd, true, 1)
		else:
			$"%GameEndLabel".text = GameVariables.GameEndUser + " has won"

func unselect_outposts(exception : String = "") -> void:
	# Hide outpost panels
	show_outpost_panels([], self)
	# Set all outposts unfocused
	for child in $Items/Outposts.get_children():
		if child.name != exception:
			child.deselect()
	# Set hovered end outpost
	if HoveredEndOutpost != null:
		if HoveredEndOutpost.name != exception:
			HoveredEndOutpost = null
	# Hide ruler
	$Ruler.hide()

func unselect_submarines(exception : String = "") -> void:
	# Hide submarine panels
	show_submarine_panels([], self)
	# Set all submarines unfocused
	for child in $Items/Submarines.get_children():
		if child.name != exception:
			child.deselect()

func show_outpost_panels(panels : Array, outpostnode : Node) -> void:
	for panelnode in $Interface/Panels/Outposts.get_children():
		panelnode.OutpostNode = outpostnode
		panelnode.visible = panels.has(panelnode.name)

func show_submarine_panels(panels : Array, submarinenode : Node) -> void:
	for panelnode in $Interface/Panels/Submarines.get_children():
		panelnode.SubmarineNode = submarinenode
		panelnode.visible = panels.has(panelnode.name)

# TODO: rework
func update_online_players() -> void:
	# Remove existing online indicators
	for child in $Interface/OnlinePlayers/OnlineIndicators.get_children():
		child.queue_free()
	# Get online players
	var OnlinePlayers : Array
	for playernode in get_node("/root/Game/Items/Players").get_children():
		var User : String = playernode.CreateItemOrder["User"]
		if User != Settings.User:
			if GameVariables.OnlineUsers.has(User):
				OnlinePlayers.append(playernode.name)
	# Set online indicators
	for i in len(OnlinePlayers):
		# Set online indicator instance
		var OnlineIndicatorInstance : Node = OnlineIndicatorScene.instantiate()
		OnlineIndicatorInstance.position.x = -25 * i
		OnlineIndicatorInstance.modulate = Color(GameVariables.Colors[OnlinePlayers[i]])
		$Interface/OnlinePlayers/OnlineIndicators.add_child(OnlineIndicatorInstance)
		$Interface/OnlinePlayers/OnlinePlayersLabel.position.x = -90 - 25 * (i + 1)
	$Interface/OnlinePlayers/OnlinePlayersLabel.visible = not OnlinePlayers.is_empty()

# Map buttons

func _on_MapButton_button_down() -> void:
	MapCursorMovementMaximum = 0
	PressedMapCursorPosition = get_global_mouse_position()
	PreviousCameraPosition = $Camera2D.global_position

func _on_MapButton_button_up() -> void:
	if MapCursorMovementMaximum <= MapCursorMovementAllowance:
		# Hide bottom bar panels
		for buttonnode in $Interface/BottomBar/HBoxContainer.get_children():
			buttonnode.button_pressed = false
		# Unselect all items
		unselect_outposts()
		unselect_submarines()
		# Set visible tick to global tick
		GameVariables.VisibleTick = GameVariables.GlobalTick
		update_tick()

# Bottom bar buttons

func _on_ExitButton_toggled(buttonpressed : bool) -> void:
	# Set exit game popup visible
	%ExitGamePopup.visible = buttonpressed

# Tweens

func _on_TickTween_finished() -> void:
	GameVariables.Jumping = false

func _on_MapTween_finished() -> void:
	if $MapControls/MapButton.button_pressed:
		PressedMapCursorPosition = get_global_mouse_position()

# Timers

func _on_TickTimer_timeout() -> void:
	set_global_tick(GameVariables.GlobalTick + 1)
	$TickTimer.start(GameVariables.TickLength)

# Visibility

func _on_Game_draw() -> void:
	update_map()
