extends Node2D

func update_active_items_tick() -> void:
	for itemnode in get_children():
		if itemnode.Active:
			itemnode.update_tick()

func calculate_active_items_history_expansion(tick : int) -> void:
	for itemnode in get_children():
		if itemnode.Active:
			itemnode.calculate_history_expansion(tick)

func calculate_active_items_history_effect(tick : int) -> void:
	for itemnode in get_children():
		if itemnode.Active:
			itemnode.calculate_history_effect(tick)

func set_items_active_false(tick : int) -> void:
	for itemnode in get_children():
		itemnode.remove_active(tick)
