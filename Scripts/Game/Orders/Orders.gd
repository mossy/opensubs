extends Node

var OrderScenes : Dictionary

# Node functions

func get_order_time() -> int:
	var OrderTime : int = -1
	if GameVariables.StartTime != -1:
		OrderTime = Time.get_unix_time_from_system()
	return OrderTime

func create_order(order : Dictionary) -> void:
	# Get order instance
	var OrderInstance : Node = OrderScenes[order["Type"]].instantiate()
	# Set order instance variables
	OrderInstance.name = str(order["OrderId"])
	OrderInstance.Order = order
	# Get order tick node
	var OrderTickNode : Node = get_node(str(order["OrderTick"]))
	if OrderTickNode == null:
		OrderTickNode = Node2D.new()
		OrderTickNode.name = str(order["OrderTick"])
		add_child(OrderTickNode)
	# Add order instance as order tick node child
	OrderTickNode.add_child(OrderInstance)

# Set initial order functions

func set_initial_orders() -> void:
	return
