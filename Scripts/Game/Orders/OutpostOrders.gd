extends "res://Scripts/Game/Orders/Orders.gd"

var OutpostNamesList : Array
var PoissonDiscSampleLimit = 30
var CurrentPlayerPoint : Vector2

# Inbuilt functions

func _ready():
	# Set order scenes
	OrderScenes = {
		"CreateOutpost" : preload("res://Scenes/Orders/Outposts/CreateOutpostOrder.tscn"),
		"TransformOutpost" : preload("res://Scenes/Orders/Outposts/TransformOutpostOrder.tscn")
	}
	# Set outpost names
	var OutpostList
	match GameVariables.OutpostNames:
		"Classics":
			OutpostList = preload("res://Names/Classics.gd").new()
		"Capitals":
			OutpostList = preload("res://Names/Capitals.gd").new()
	OutpostNamesList = OutpostList.Names

# Set initial order functions

func set_initial_orders() -> void:
	# Set outposts variables
	GameVariables.OutpostsTotal = GameVariables.PlayerCount * GameVariables.OutpostsPerPlayer
	GameVariables.DominationVictoryTotal = GameVariables.OutpostsTotal * GameVariables.DominationVictoryPercentage / 100
	# Get outpost points
	var MapLength : int = ceil(sqrt(GameVariables.OutpostsTotal)) * GameVariables.OutpostSpacing
	var OutpostPoints : Array
	match GameVariables.MapType:
		"Grid":
			GameVariables.MapSize = Vector2(MapLength, MapLength)
			GameVariables.HalfMapSize = GameVariables.MapSize / 2
			OutpostPoints = calculate_grid_points(GameVariables.OutpostsTotal, GameVariables.MapSize, GameVariables.OutpostSpacing)
		"RandomSpread":
			MapLength *= 1.35
			GameVariables.MapSize = Vector2(MapLength, MapLength)
			GameVariables.HalfMapSize = GameVariables.MapSize / 2
			OutpostPoints = calculate_random_spread_points(GameVariables.OutpostsTotal, GameVariables.MapSize, GameVariables.OutpostSpacing)
	# TODO: implement
	# Get player points
	var PlayerPoints : Array
	var PlayerSpacing : float = GameVariables.MapSize.x / ceil(sqrt(GameVariables.PlayerCount)) / 1.15
	match "RandomSpread":
		"RandomSpread":
			PlayerPoints = calculate_random_spread_points(GameVariables.PlayerCount, GameVariables.MapSize, PlayerSpacing)
	# Get player outpost points
	var PlayerNodes : Array = get_node("/root/Game/Items/Players").get_children()
	var PlayerOutpostPoints : Dictionary
	for playerpoint in PlayerPoints:
		# Get list of outpost points sorted by distance to player point
		CurrentPlayerPoint = playerpoint
		var SortedClosePoints : Array = OutpostPoints.duplicate(true)
		SortedClosePoints.sort_custom(sort_player_outpost_distance)
		# Get player
		var PlayerNode : Node = PlayerNodes[randi_range(0, len(PlayerNodes) - 1)]
		var Player : String = PlayerNode.name
		PlayerNodes.erase(PlayerNode)
		# Set starting outpost points
		var StartingPoints : Array = SortedClosePoints.slice(len(SortedClosePoints) - 4, len(SortedClosePoints))
		for startingpoint in StartingPoints:
			OutpostPoints.erase(startingpoint)
		PlayerOutpostPoints[Player] = StartingPoints
	PlayerOutpostPoints["Dormant"] = OutpostPoints
	# Set create outpost orders
	for player in PlayerOutpostPoints:
		for i in len(PlayerOutpostPoints[player]):
			# Get initial values
			var InitialTroops : int
			if player != "Dormant":
				InitialTroops = GameVariables.InitialOutpostTroops
			var InitialType : String
			if i % 2 == 0:
				InitialType = "Factory"
			else:
				InitialType = "Generator"
			# Get create outpost order
			var CreateOutpostOrder : Dictionary = {
				# Order variables
				"Time" : -1,
				"Type" : "CreateOutpost",
				"OrderTick" : 0,
				"OrderId" : randi(),
				# Create outpost order variables
				"Outpost" : "",
				"ShieldMaximum" : [10, 20][randi_range(0, 1)],
				"InitialPosition" : PlayerOutpostPoints[player][i],
				"InitialTroops" : InitialTroops,
				"InitialPlayer" : player,
				"InitialType" : InitialType
			}
			# Set create outpost order
			create_order(CreateOutpostOrder)
	print("Successfully generated " + str(get_node("0").get_child_count()) + " outposts")

func sort_player_outpost_distance(a : Vector2, b : Vector2) -> bool:
	var AWrappedPosition : Vector2 = get_node("/root/Game").get_closest_wrapped_position(CurrentPlayerPoint, a)
	var BWrappedPosition : Vector2 = get_node("/root/Game").get_closest_wrapped_position(CurrentPlayerPoint, b)
	return CurrentPlayerPoint.distance_to(AWrappedPosition) > CurrentPlayerPoint.distance_to(BWrappedPosition)

func calculate_grid_points(pointstotal : int, area : Vector2, spacing : int) -> Array:
	var Points : Array
	for x in int(area.x / spacing):
		for y in int(area.y / spacing):
			Points.append(Vector2(x * spacing, y * spacing) - GameVariables.HalfMapSize)
			if len(Points) >= pointstotal:
				break
	return Points

func calculate_random_spread_points(pointstotal : int, area : Vector2, spacing : int) -> Array:
	var Points : Array
	var SpawnPoints : Array
	# Create grid
	var Grid : Array
	var CellSize : int = spacing / sqrt(2)
	var GridSize : int = area.x / CellSize
	for x in GridSize:
		Grid.append([])
		for y in GridSize:
			Grid[x].append(-1)
	# Create first point
	var FirstPoint : Vector2 = Vector2(randi_range(0, GridSize * CellSize), randi_range(0, GridSize * CellSize))
	Points.append(FirstPoint - GameVariables.HalfMapSize)
	SpawnPoints.append(FirstPoint)
	var GridPosition : Vector2 = Vector2(int(SpawnPoints[0].x / CellSize), int(SpawnPoints[0].y / CellSize))
	Grid[GridPosition.x][GridPosition.y] = 0
	# Generate points
	while SpawnPoints.is_empty() == false and len(Points) < pointstotal: 
		var CandidateAccepted : bool = false
		# Get random spawn point
		var SpawnPointsIndex : int = randi_range(0, len(SpawnPoints) - 1)
		# Attempt to find new valid point adjacent to spawn point
		for i in PoissonDiscSampleLimit:
			# Get candidate point
			var Angle : float = randf_range(0, TAU)
			var Distance : Vector2 = Vector2(randf_range(spacing, spacing * 2), 0) 
			var CandidatePoint : Vector2 = SpawnPoints[SpawnPointsIndex] + Distance.rotated(Angle)
			var WrappedCandidatePoint : Vector2 = get_node("/root/Game").get_wrapped_position(CandidatePoint - GameVariables.HalfMapSize, Vector2())
			var PositiveWrappedCandidatePoint : Vector2 = WrappedCandidatePoint + GameVariables.HalfMapSize
			# Get candidate point position on grid
			GridPosition = Vector2(posmod(PositiveWrappedCandidatePoint.x / CellSize, GridSize), posmod(PositiveWrappedCandidatePoint.y / CellSize, GridSize))
			# Check that grid point is empty
			if Grid[GridPosition.x][GridPosition.y] == -1:
				# Get all adjacent points
				var NearbyPoints : Array
				for x in 5:
					for y in 5:
						var NearbyPoint : Vector2 = Vector2(posmod(GridPosition.x - 2 + x, GridSize), posmod(GridPosition.y - 2 + y, GridSize))
						if Grid[NearbyPoint.x][NearbyPoint.y] != -1:
							NearbyPoints.append(Grid[NearbyPoint.x][NearbyPoint.y])
				# Check if nearby points are too close
				var PointValid : bool = true
				for checkpoint in NearbyPoints:
					var CheckPointWrapped : Vector2 = get_node("/root/Game").get_closest_wrapped_position(WrappedCandidatePoint, Points[checkpoint])
					if WrappedCandidatePoint.distance_to(CheckPointWrapped) < spacing:
						PointValid = false
						break
				if PointValid:
					# Add candidate points to points
					SpawnPoints.append(PositiveWrappedCandidatePoint)
					Points.append(WrappedCandidatePoint)
					Grid[GridPosition.x][GridPosition.y] = len(Points) - 1 
					CandidateAccepted = true
					# Check is points are full
					if len(Points) >= pointstotal:
						break
			else:
				continue
		# Remove point with too many surrounding other points
		if CandidateAccepted == false:
			SpawnPoints.remove_at(SpawnPointsIndex)
	return Points
