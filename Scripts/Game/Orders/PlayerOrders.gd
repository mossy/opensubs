extends "res://Scripts/Game/Orders/Orders.gd"

var PlayerNamesList : Array = [
	"Red",
	"Orange",
	"Cyan",
	"Sky",
	"Blue",
	"Purple",
	"Pink",
	"Olive",
	"Beige",
	"Brown"
]

# Inbuilt functions

func _ready() -> void:
	# Set order scenes
	OrderScenes = {
		"CreatePlayer" : preload("res://Scenes/Orders/Players/CreatePlayerOrder.tscn")
	}

# Set initial order functions

func set_initial_orders() -> void:
	# Set create outpost orders
	for i in GameVariables.PlayerCount:
		# Get create player order
		var CreatePlayerOrder : Dictionary = {
			# Order variables
			"Time" : -1,
			"Type" : "CreatePlayer",
			"OrderTick" : 0,
			"OrderId" : randi(),
			"User" : "Empty",
			# Create player order variables
			"Player" : ""
		}
		# Set user
		if i < len(GameVariables.Users):
			CreatePlayerOrder["User"] = GameVariables.Users[i]
		# Set create player order
		create_order(CreatePlayerOrder)

# Set order variable functions

func set_order_user(user : String) -> void:
	for ordernode in get_node("0").get_children():
		var Order : Dictionary = ordernode.Order
		if Order["User"] == "Empty":
			Order["User"] = user
			return
