extends "res://Scripts/Game/Orders/Orders.gd"

# Inbuilt functions

func _ready() -> void:
	# Set order scenes
	OrderScenes = {
		"CreateSubmarine" : preload("res://Scenes/Orders/Submarines/CreateSubmarineOrder.tscn"),
		"GiftSubmarine" : preload("res://Scenes/Orders/Submarines/GiftSubmarineOrder.tscn")
	}
