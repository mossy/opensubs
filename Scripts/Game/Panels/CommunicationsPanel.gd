extends Control

var ChatButtonScene : PackedScene = preload("res://Scenes/InterfaceElements/ChatButton.tscn")
var UserPillsContainerScene : PackedScene = preload("res://Scenes/InterfaceElements/UserPillsContainer.tscn")
var UserPillScene : PackedScene = preload("res://Scenes/InterfaceElements/UserPill.tscn")
var MessageScene : PackedScene = preload("res://Scenes/InterfaceElements/Message.tscn")

var CurrentUsers : Array
var MaximumScroll : int

func _ready() -> void:
	MaximumScroll = $Messages/ScrollContainer.get_v_scroll_bar().max_value
	$Messages/ScrollContainer.get_v_scroll_bar().connect("changed", jump_to_messages_end)

func _process(delta : float) -> void:
	if visible:
		if $Messages/TextEdit.has_focus():
			if Input.is_action_just_pressed("SendMessage"):
				$Messages/TextEdit.text[-1] = ""
				send_message()

func update_chats() -> void:
	for child in $Chats/ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("ChatButton"):
			child.queue_free()
	for users in GameVariables.Chats:
		# Set chat button instance
		var ChatButtonInstance : Node = ChatButtonScene.instantiate()
		ChatButtonInstance.Users = users
		ChatButtonInstance.set_message(GameVariables.Chats[users][-1])
		$Chats/ScrollContainer/VBoxContainer.add_child(ChatButtonInstance)

func set_players(users : Array, userselection : bool = false) -> void:
	if userselection:
		CurrentUsers.append(Settings.User)
		# Get user pills
		var UserPills : Array
		for user in GameVariables.Users:
			if user != Settings.User:
				var UserPillInstance : Node = UserPillScene.instantiate()
				UserPillInstance.User = user
				UserPills.append(UserPillInstance)
		# Check that user pills are not empty
		if CurrentUsers.is_empty() == false:
			# Add user pills to rows
			add_user_pills_to_rows(UserPills)
		else:
			# Show abset chat players popup
			get_node("/root/Game/%AbsentChatPlayersPopup").show()
			$Chats.show()
			$Messages.hide()
	else:
		# Get user pills
		var UserPills : Array
		for user in users:
			if user != Settings.User:
				var UserPillInstance : Node = UserPillScene.instantiate()
				UserPillInstance.User = user
				UserPillInstance.disabled = true
				UserPills.append(UserPillInstance)
		# Add user pills to row
		add_user_pills_to_rows(UserPills)

func set_messages(users : Array) -> void:
	CurrentUsers = users.duplicate(true)
	for child in $Messages/ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("Message"):
			child.queue_free()
	if GameVariables.Chats.has(users):
		for message in GameVariables.Chats[users]:
			# Set message instance
			var MessageInstance : Node = MessageScene.instantiate()
			MessageInstance.set_message(message)
			$Messages/ScrollContainer/VBoxContainer.add_child(MessageInstance)
	elif users != [Settings.User]:
		# Failure case
		print(str(users) + " are not in an existing chat")

func set_chat(players : Array, playerselection : bool = false) -> void:
	$Chats.hide()
	$Messages.show()
	set_players(players, playerselection)
	set_messages(players)

func add_user_pills_to_rows(userpills : Array) -> void:
	# Remove existing user pills
	for child in $Messages/LargePanelHeader/UserPills.get_children():
		child.queue_free()
	# Set user pills container instance
	var UserPillsContainerInstance : Node = UserPillsContainerScene.instantiate()
	$Messages/LargePanelHeader/UserPills.add_child(UserPillsContainerInstance) 
	# Set user pills
	for userpill in userpills:
		# Get user pills width
		var UserPillWidth : int = userpill.theme.get_font("label", "Font").get_string_size(userpill.User).x + 10
		var UserPillRowWidth : int = UserPillWidth
		for child in $Messages/LargePanelHeader/UserPills.get_children()[-1].get_children():
			UserPillRowWidth += child.size.x
		# Set secondary user pills container instance
		if UserPillRowWidth > $Messages/LargePanelHeader/UserPills.size.x:
			var SecondaryUserPillsContainerInstance : Node = UserPillsContainerScene.instantiate()
			$Messages/LargePanelHeader/UserPills.add_child(SecondaryUserPillsContainerInstance)
		$Messages/LargePanelHeader/UserPills.get_children()[-1].add_child(userpill)

func send_message() -> void:
	# Checks that message is valid
	if (
		$Messages/TextEdit.text != "" and
		CurrentUsers != [Settings.User]
	):
		var NewMessage : Dictionary = {
			"Users" : [Settings.User],
			"Text" : $Messages/TextEdit.text,
			"Time" : Time.get_unix_time_from_system()
		}
		Network.rpc_id(
			1,
			"send_message",
			# Game ID
			GameVariables.GameId,
			# Client ID
			multiplayer.get_unique_id(),
			# Chat
			CurrentUsers,
			# Message
			NewMessage
		)
		# Adds new message to chats
		if GameVariables.Chats.has(CurrentUsers) == false:
			GameVariables.Chats[CurrentUsers.duplicate(true)] = []
		GameVariables.Chats[CurrentUsers].append(NewMessage)
		# Updates to display new message
		set_chat(CurrentUsers)
		# Clear text input for new message
		$Messages/TextEdit.text = ""

func update_messages(users : Array) -> void:
	if visible:
		if users == CurrentUsers:
			set_messages(CurrentUsers)
		update_chats()

func jump_to_messages_end() -> void:
	if $Messages/ScrollContainer.get_v_scroll_bar().max_value != MaximumScroll:
		MaximumScroll = $Messages/ScrollContainer.get_v_scroll_bar().max_value
		$Messages/ScrollContainer.scroll_vertical = MaximumScroll

func _on_CreateChatButton_pressed() -> void:
	$Chats.hide()
	$Messages.show()
	set_chat([Settings.User], true)

func _on_BackButton_pressed() -> void:
	$Chats.show()
	$Messages.hide()
	update_chats()

func _on_SendButton_pressed() -> void:
	send_message()

func _on_CommunicationsPanel_visibility_changed() -> void:
	if visible:
		$Chats.show()
		$Messages.hide()
		update_chats()
