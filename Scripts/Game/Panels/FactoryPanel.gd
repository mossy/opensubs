extends "res://Scripts/Game/Panels/OutpostPanel.gd"

func set_actions() -> void:
	super()
	# Get outpost data
	var OutpostHistoryEntry : Dictionary = OutpostNode.get_history_entry(GameVariables.VisibleTick)
	# Set jump to production action visible
	$Actions/JumpToProduction.visible = OutpostHistoryEntry["CurrentPlayer"] != "Dormant"

func get_info() -> String:
	# Get outpost data
	var OutpostHistoryEntry : Dictionary = OutpostNode.get_history_entry(GameVariables.VisibleTick)
	# Get generate troops ticks
	var GenerateTroopsTicks : int = OutpostNode.get_generate_troops_ticks(GameVariables.VisibleTick)
	var RemainingGenerateTroopsTicks : int = OutpostHistoryEntry["TroopsTicksCountdown"]
	if RemainingGenerateTroopsTicks == 0:
		RemainingGenerateTroopsTicks = GenerateTroopsTicks
	# Return outpost info
	return (
		"\n+" + str(GameVariables.GenerateTroopsNumber * (1 + OutpostHistoryEntry["TroopsNumberMultipliersTotal"])) +
		" in " + VariableFunctions.get_time_text(RemainingGenerateTroopsTicks * GameVariables.TickLength) +
		" and every " + VariableFunctions.get_time_text(GenerateTroopsTicks * GameVariables.TickLength)
	)

func _on_JumpToProductionButton_pressed() -> void:
	# Get outpost data
	var OutpostNextHistoryEntry : Dictionary = OutpostNode.get_history_entry(GameVariables.VisibleTick + 1)
	# Jump to next generate troops tick
	get_node("/root/Game").jump_to_tick(GameVariables.VisibleTick + OutpostNextHistoryEntry["TroopsTicksCountdown"] + 1)
