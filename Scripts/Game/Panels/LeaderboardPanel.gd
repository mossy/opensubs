extends Panel

var LeaderboardEntryScene : PackedScene = preload("res://Scenes/InterfaceElements/LeaderboardEntry.tscn")

var InfoType : String = "Troops"

func _ready() -> void:
	$PlayerInfoTypes/Ore.visible = GameVariables.VictoryType == "Mining"

func create_leaderboard_entry(playernode : Node) -> void:
	# Set player leaderboard entry instance
	var PlayerLeaderboardEntryInstance : Node = LeaderboardEntryScene.instantiate()
	PlayerLeaderboardEntryInstance.PlayerNode = playernode
	$ScrollContainer/VBoxContainer.add_child(PlayerLeaderboardEntryInstance)
	$ScrollContainer/VBoxContainer.move_child(PlayerLeaderboardEntryInstance, 0)

func update_tick() -> void:
	for child in $ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("Permanent") == false:
			child.update_tick()

func set_info_type(infotype : String) -> void:
	InfoType = infotype
	for child in $PlayerInfoTypes.get_children():
		child.button_pressed = child.name == InfoType
	update_tick()

func _on_Ore_pressed() -> void:
	set_info_type("Ore")

func _on_Outposts_pressed() -> void:
	set_info_type("Outposts")

func _on_Troops_pressed() -> void:
	set_info_type("Troops")

func _on_LeaderboardPanel_visibility_changed() -> void:
	if visible:
		set_info_type("Troops")
