extends "res://Scripts/Game/Panels/OutpostPanel.gd"

func set_actions() -> void:
	return

func _on_JumpToProductionButton_pressed() -> void:
	# Get outpost data
	var OutpostNextHistoryEntry : Dictionary = OutpostNode.get_history_entry(GameVariables.VisibleTick + 1)
	# Jump to next generate ore tick
	get_node("/root/Game").jump_to_tick(GameVariables.VisibleTick + OutpostNextHistoryEntry["OreTicksCountdown"] + 1)
