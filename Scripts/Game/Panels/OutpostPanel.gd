extends Panel

var OutpostNode : Node
var Outpost : String

var CreateOupostOrderNode : Node
var CreateOupostOrder : Dictionary

func update_tick() -> void:
	if visible:
		# Get outpost data
		var OutpostHistoryEntry : Dictionary = OutpostNode.get_history_entry(GameVariables.VisibleTick)
		var OutpostPlayer : String = OutpostHistoryEntry["CurrentPlayer"]
		var OutpostUser : String
		if OutpostPlayer != "Dormant":
			var OutpostPlayerNode : Node = get_node("/root/Game/Items/Players/" + OutpostPlayer)
			OutpostUser = OutpostPlayerNode.CreateItemOrder["User"]
		# Set header elements
		var PlayerColor : Color = Color(GameVariables.Colors[OutpostPlayer])
		$ItemPanelHeader.theme.get_stylebox("panel", "Panel").bg_color = PlayerColor
		$ItemPanelHeader/ItemLabel.text = Outpost.to_upper()
		$ItemPanelHeader/PlayerLabel.text = OutpostUser
		$ItemPanelHeader/PlayerLabel.visible = OutpostUser != "Empty"
		# Sets outpost info
		$Info/TroopsLabel.text = str(OutpostHistoryEntry["TroopsTotal"])
		$Info/InfoLabel.text = "Drillers" + get_info()
		# Sets toast panel elements
		set_actions()
		set_toasts()
		update_toasts()

func set_actions() -> void:
	# Get outpost data
	var OutpostHistoryEntry : Dictionary = OutpostNode.get_history_entry(GameVariables.VisibleTick)
	# Set mine action
	$Actions/Mine.visible = (
		GameVariables.VictoryType == "Mining" and
		OutpostHistoryEntry["CurrentPlayer"] == GameVariables.Player and
		OutpostHistoryEntry["CurrentDetector"] != "" and
		GameVariables.VisibleTick >= GameVariables.GlobalTick
	)
	var TransformOrderNode : Node = OutpostNode.get_previous_item_order_nodes(GameVariables.VisibleTick, "TransformOutpost", true)[-1]
	$Actions/Mine/Button.disconnect("toggled", _on_MineButton_toggled)
	if TransformOrderNode.is_inside_tree() and TransformOrderNode.InvalidReasons.has("Canceled") == false:
		var TransformOrder : Dictionary = TransformOrderNode.Order
		$Actions/Mine.visible = $Actions/Mine.visible and OutpostHistoryEntry["CurrentType"] != "Mine"
		$Actions/Mine/Button.button_pressed = TransformOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick)
	else:
		$Actions/Mine/Button.button_pressed = false
	$Actions/Mine/Button.connect("toggled", _on_MineButton_toggled)

func get_info() -> String:
	return ""

# Gets toasts that should be visible
func set_toasts() -> void:
	# Get outpost data
	var OutpostHistoryEntry : Dictionary = OutpostNode.get_history_entry(GameVariables.VisibleTick)
	# Hide toasts
	for toastnode in $Toasts.get_children():
		toastnode.hide()
	# Check that transform order exists
	var TransformOrderNode : Node = OutpostNode.get_previous_item_order_nodes(GameVariables.VisibleTick, "TransformOutpost", true)[-1]
	if TransformOrderNode.is_inside_tree() and TransformOrderNode.InvalidReasons.has("Canceled") == false:
		# Check that transform outpost order is within wait period
		if TransformOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick):
			$Toasts/Transforming.show()
	# Show order reasons toasts
	for ordernode in [TransformOrderNode]:
		if ordernode.is_inside_tree() and ordernode.InvalidReasons.has("Canceled") == false:
			if ordernode.get_awaiting_order_effect(GameVariables.VisibleTick):
				for reason in ordernode.get_combined_reasons():
					if $Toasts.has_node(reason):
						$Toasts.get_node(reason).show()

func update_toasts() -> void:
	# Check that transforming toast is visible
	if $Toasts/Transforming.visible:
		var TransformOrderNode : Node = OutpostNode.get_previous_item_order_nodes(GameVariables.VisibleTick, "TransformOutpost", true)[-1]
		if TransformOrderNode.is_inside_tree() and TransformOrderNode.InvalidReasons.has("Canceled") == false:
			# Get time to transform outpost order effect
			var TimeToTransformEffect : int = TransformOrderNode.get_time_to_order_effect(GameVariables.VisibleTick)
			$Toasts/Transforming/Label.text = "Transforming in " + VariableFunctions.get_time_text(TimeToTransformEffect)

func _on_OutpostPanel_visibility_changed() -> void:
	if visible:
		# Set outpost data
		Outpost = OutpostNode.name
		# Set create outpost order data
		CreateOupostOrderNode = OutpostNode.CreateItemOrderNode
		CreateOupostOrder = OutpostNode.CreateItemOrder
		# Update tick
		update_tick()

func _on_MineButton_toggled(buttonpressed : bool) -> void:
	# Check if visible tick is valid for order
	if GameVariables.VisibleTick >= GameVariables.GlobalTick:
		# Set mine order
		if buttonpressed:
			# Get transform outpost order
			var OutpostOrdersNode : Node = get_node("/root/Game/Orders/Outposts/")
			var TransformOutpostOrder : Dictionary = {
				# Order variables
				"Time" : OutpostOrdersNode.get_order_time(),
				"Type" : "TransformOutpost",
				"OrderTick" : GameVariables.VisibleTick,
				"OrderId" : randi(),
				"User" : Settings.User,
				# Outpost order variables
				"CreateOutpostOrderTick" : CreateOupostOrder["OrderTick"],
				"CreateOutpostOrderId" : CreateOupostOrder["OrderId"],
				# Create outpost order variables
				"Outpost" : Outpost,
				"TransformType" : "Mine"
			}
			# Set transform outpost order
			Network.rpc_id(
				1,
				"set_server_order",
				# Game ID
				GameVariables.GameId,
				# Client ID
				multiplayer.get_unique_id(),
				# Order
				TransformOutpostOrder
			)
			OutpostOrdersNode.create_order(TransformOutpostOrder)
		else:
			# Get existing mine order
			var TransformOutpostOrderNode : Node = OutpostNode.get_previous_item_order_nodes(GameVariables.VisibleTick, "TransformOutpost", true)[-1]
			# Remove mine order
			TransformOutpostOrderNode.remove_order()
