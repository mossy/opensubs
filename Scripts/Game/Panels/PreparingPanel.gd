extends Panel

# Create submarine order variables

var CreateSubmarineOrderNode : Node
var CreateSubmarineOrder : Dictionary

# Submarine variables

var SubmarineNode : Node
var SubmarineHistory : Array
var Submarine : String

# Initial outpost variables

var InitialOutpostNode : Node
var InitialOutpostHistory : Array
var InitialOutpost : String

var ShowTemporaryTroops : bool = false

func _process(delta : float) -> void:
	if visible:
		if $ScrollWheel.button_pressed:
			if Settings.CalculateTroopsDelay == -1:
				update_initial_troops()
			else:
				$CalculateTimer.start(Settings.CalculateTroopsDelay)

func update_tick() -> void:
	if visible:
		# Get initial outpost data
		var InitialOutpostHistoryEntry : Dictionary = InitialOutpostNode.get_history_entry(GameVariables.VisibleTick)
		# Set header elements
		var PlayerColor : Color = Color(GameVariables.Colors[GameVariables.Player])
		$ItemPanelHeader.theme.get_stylebox("panel", "Panel").bg_color = PlayerColor
		$ItemPanelHeader/ItemLabel.text = InitialOutpost.to_upper()
		# Sets scroll wheel
		$ScrollWheel.MaximumValue = InitialOutpostHistoryEntry["TroopsTotal"] + CreateSubmarineOrder["InitialTroops"]
		# Set troops text
		if ShowTemporaryTroops == false:
			$TroopsLabel.text = str(InitialOutpostHistoryEntry["TroopsTotal"])
		# Hide unused buttons
		hide_unusued_buttons()

func hide_unusued_buttons() -> void:
	return

func update_initial_troops() -> void:
	# Stops calculate timer
	$CalculateTimer.disconnect("timeout", _on_CalculateTimer_timeout)
	$CalculateTimer.stop()
	$CalculateTimer.connect("timeout", _on_CalculateTimer_timeout)
	# If scroll wheel has changed, update submarine troops
	if $ScrollWheel.StepValue != CreateSubmarineOrder["InitialTroops"]:
		# Set create submarine order
		Network.rpc_id(
			1,
			"set_server_create_submarine_order_initial_troops",
			# Game ID
			GameVariables.GameId,
			# Client ID
			multiplayer.get_unique_id(),
			# Order tick
			CreateSubmarineOrder["OrderTick"],
			# Order ID
			CreateSubmarineOrder["OrderId"],
			# Troops
			$ScrollWheel.StepValue
		)
		# Set create submarine order node
		CreateSubmarineOrderNode.set_initial_troops($ScrollWheel.StepValue)

func set_temporary_troops(troops : int) -> void:
	ShowTemporaryTroops = true
	$TroopsLabel.text = str(troops)

func _on_PreparingPanel_visibility_changed() -> void:
	if visible:
		ShowTemporaryTroops = false
		# Set submarine data
		SubmarineHistory = SubmarineNode.History
		Submarine = SubmarineNode.name
		# Set initial outpost data
		InitialOutpostNode = SubmarineNode.InitialOutpostNode
		InitialOutpostHistory = InitialOutpostNode.History
		InitialOutpost = InitialOutpostNode.name
		# Set create submarine order data
		CreateSubmarineOrderNode = SubmarineNode.CreateItemOrderNode
		CreateSubmarineOrder = CreateSubmarineOrderNode.Order
		# Get submarine data
		var SubmarineHistoryEntry : Dictionary = SubmarineNode.get_history_entry(GameVariables.VisibleTick)
		# Update tick
		update_tick()
		# Set scroll wheel
		if $ScrollWheel.StepValue != SubmarineHistoryEntry["TroopsTotal"]:
			if $CalculateTimer.is_stopped():
				$ScrollWheel.set_value(SubmarineHistoryEntry["TroopsTotal"])
				$ScrollWheel.update_notch_positions()

func _on_CancelButton_pressed() -> void:
	CreateSubmarineOrderNode.remove_order()
	$CalculateTimer.stop()

func _on_CalculateTimer_timeout() -> void:
	update_initial_troops()

func _on_ScrollWheel_value_changed(value : float) -> void:
	# Get initial outpost data
	var InitialOutpostHistoryEntry : Dictionary = InitialOutpostNode.get_history_entry(GameVariables.VisibleTick)
	# Get temporary troops
	var TemporaryTroops : int = value
	var TemporaryRemainingTroops : int = InitialOutpostHistoryEntry["TroopsTotal"] + CreateSubmarineOrder["InitialTroops"] - TemporaryTroops
	# Set temporary troops
	get_node("/root/Game/Items/Submarines/" + Submarine).set_temporary_troops(TemporaryTroops)
	get_node("/root/Game/Items/Outposts/" + InitialOutpost).set_temporary_troops(TemporaryRemainingTroops)
	get_node("/root/Game/%LaunchingPanel").set_temporary_troops(TemporaryTroops)
	set_temporary_troops(TemporaryRemainingTroops)
