extends Panel

var SubmarineNode : Node
var SubmarineHistory : Array
var Submarine : String

var CreateSubmarineOrderNode : Node
var CreateSubmarineOrder : Dictionary

var ShowTemporaryTroops : bool = false

func _process(delta : float) -> void:
	if visible:
		# Update toasts
		update_toasts()

func update_tick() -> void:
	if visible:
		# Get submarine data
		var SumbarineHistoryEntry = SubmarineNode.get_history_entry(GameVariables.VisibleTick)
		var SubmarinePlayer : String = SumbarineHistoryEntry["CurrentPlayer"]
		var SubmarinePlayerNode : Node = get_node("/root/Game/Items/Players/" + SubmarinePlayer)
		var SubmarineUser : String = SubmarinePlayerNode.CreateItemOrder["User"]
		# Set header elements
		var PlayerColor : Color = Color(GameVariables.Colors[SubmarinePlayer])
		$ItemPanelHeader.theme.get_stylebox("panel", "Panel").bg_color = PlayerColor
		$ItemPanelHeader/PlayerLabel.text = SubmarineUser
		# Sets submarine info
		$Info/InfoLabel.text = get_info()
		# Sets troop panel elements
		if ShowTemporaryTroops == false:
			if CreateSubmarineOrderNode.InvalidReasons.is_empty():
				$Info/TroopsLabel.text = str(SumbarineHistoryEntry["TroopsTotal"])
			else:
				$Info/TroopsLabel.text = str(CreateSubmarineOrder["InitialTroops"])
		# Sets toast panel elements
		set_actions()
		set_toasts()
		update_toasts()
		# Sets submarine label text
		if SumbarineHistoryEntry["CurrentType"] == "Gift":
			$ItemPanelHeader/ItemLabel.text = "SUB (GIFT)"
		else:
			$ItemPanelHeader/ItemLabel.text = "SUB"

func set_actions() -> void:
	# Get submarine data
	var SumbarineHistoryEntry = SubmarineNode.get_history_entry(GameVariables.VisibleTick)
	# Set gift action
	$Actions/Gift.visible = (
		SumbarineHistoryEntry["CurrentPlayer"] == GameVariables.Player and
		SumbarineHistoryEntry["CurrentDetector"] != "" and
		GameVariables.VisibleTick >= GameVariables.GlobalTick
	)
	var GiftSubmarineOrderNode : Node = SubmarineNode.get_previous_item_order_nodes(GameVariables.VisibleTick, "GiftSubmarine", true)[-1]
	$Actions/Gift/Button.disconnect("toggled", _on_GiftButton_toggled)
	if GiftSubmarineOrderNode.is_inside_tree() and GiftSubmarineOrderNode.InvalidReasons.has("Canceled") == false:
		var GiftOrder : Dictionary = GiftSubmarineOrderNode.Order
		$Actions/Gift.visible = $Actions/Gift.visible and SumbarineHistoryEntry["CurrentType"] != "Gift"
		$Actions/Gift/Button.button_pressed = GiftSubmarineOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick)
	else:
		$Actions/Gift/Button.button_pressed = false
	$Actions/Gift/Button.connect("toggled", _on_GiftButton_toggled)

# Gets toasts that should be visible
func set_toasts() -> void:
	# Get submarine data
	var SubmarineHistoryEntry : Dictionary = SubmarineNode.get_history_entry(GameVariables.VisibleTick)
	# Hide toasts
	for toastnode in $Toasts.get_children():
		toastnode.hide()
	# Check that create submarine order is within wait period
	if CreateSubmarineOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick):
		$Toasts/Launching.show()
	# Check that gift order exists
	var GiftOrderNode : Node = SubmarineNode.get_previous_item_order_nodes(GameVariables.VisibleTick, "GiftSubmarine", true)[-1]
	if GiftOrderNode.is_inside_tree() and GiftOrderNode.InvalidReasons.has("Canceled") == false:
		# Check that gift order is within wait period
		if GiftOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick):
			$Toasts/Gifting.show()
	# Show order reasons toasts
	for ordernode in [CreateSubmarineOrderNode, GiftOrderNode]:
		if ordernode.is_inside_tree() and ordernode.InvalidReasons.has("Canceled") == false:
			if ordernode.get_awaiting_order_effect(GameVariables.VisibleTick):
				for reason in ordernode.get_combined_reasons():
					if $Toasts.has_node(reason):
						$Toasts.get_node(reason).show()
	# Check if submarine is captured before arrival
	if SubmarineHistory[-1]["CurrentPlayer"] != SubmarineHistoryEntry["CurrentPlayer"]:
		# Show captured before arrival toast
		$Toasts/CapturedBeforeArrival.show()

func update_toasts() -> void:
	# Check that launch toast is visible
	if $Toasts/Launching.visible:
		# Get time to create submarine order effect
		var TimeToCreateSubmarineOrderEffect : int = CreateSubmarineOrderNode.get_time_to_order_effect(GameVariables.VisibleTick)
		$Toasts/Launching/Label.text = "Launching in " + VariableFunctions.get_time_text(TimeToCreateSubmarineOrderEffect)
	# Check that gift toast is visible
	if $Toasts/Gifting.visible:
		var GiftOrderNode : Node = SubmarineNode.get_previous_item_order_nodes(GameVariables.VisibleTick, "GiftSubmarine", true)[-1]
		if GiftOrderNode.is_inside_tree() and GiftOrderNode.InvalidReasons.has("Canceled") == false:
			# Get time to gift order effect
			var TimeToGiftOrderEffect : int = GiftOrderNode.get_time_to_order_effect(GameVariables.VisibleTick)
			$Toasts/Gifting/Label.text = "Gifting in " + VariableFunctions.get_time_text(TimeToGiftOrderEffect)

func set_temporary_troops(troops : int) -> void:
	ShowTemporaryTroops = true
	$Info/TroopsLabel.text = str(troops)

func get_info() -> String:
	# Get submarine data
	var SubmarineHistoryEntry : Dictionary = SubmarineNode.get_history_entry(GameVariables.VisibleTick)
	# Get info
	var Info : String = "Drillers"
	if SubmarineHistoryEntry.is_empty() == false:
		var ArrivalTick : int = SubmarineHistory[-1]["Tick"]
		var TimeToArrival : int = (ArrivalTick - GameVariables.VisibleTick) * GameVariables.TickLength
		Info += "\nArrives in " + VariableFunctions.get_time_text(TimeToArrival)
		Info += "\nSpeed multiplier " + str(1 + SubmarineHistoryEntry["SpeedMultiplierTotal"])
	return Info

func _on_SubmarinePanel_visibility_changed() -> void:
	if visible:
		ShowTemporaryTroops = false
		# Set submarine data
		SubmarineHistory = SubmarineNode.History
		Submarine = SubmarineNode.name
		# Set create submarine order data
		CreateSubmarineOrderNode = SubmarineNode.CreateItemOrderNode
		CreateSubmarineOrder = SubmarineNode.CreateItemOrder
		# Update tick
		update_tick()

func _on_JumpToArrivalButton_pressed() -> void:
	# Check if order is valid
	if CreateSubmarineOrderNode.InvalidReasons.is_empty():
		get_node("/root/Game").jump_to_arrival(Submarine)

func _on_GiftButton_toggled(buttonpressed : bool) -> void:
	# Check if visible tick is valid for order
	if GameVariables.VisibleTick >= GameVariables.GlobalTick:
		# Set gift order
		if buttonpressed:
			# Get gift submarine order
			var SubmarineOrdersNode : Node = get_node("/root/Game/Orders/Submarines/")
			var GiftOrder : Dictionary = {
				# Order variables
				"Time" : SubmarineOrdersNode.get_order_time(),
				"Type" : "GiftSubmarine",
				"OrderTick" : GameVariables.VisibleTick,
				"OrderId" : randi(),
				"User" : Settings.User,
				# Submarine order variables
				"CreateSubmarineOrderTick" : CreateSubmarineOrder["OrderTick"],
				"CreateSubmarineOrderId" : CreateSubmarineOrder["OrderId"]
			}
			# Set gift order
			Network.rpc_id(
				1,
				"set_server_order",
				# Game ID
				GameVariables.GameId,
				# Client ID
				multiplayer.get_unique_id(),
				# Order
				GiftOrder
			)
			SubmarineOrdersNode.create_order(GiftOrder)
		else:
			# Get existing gift order
			var GiftOrderNode : Node = SubmarineNode.get_previous_item_order_nodes(GameVariables.VisibleTick, "GiftSubmarine", true)[-1]
			# Remove gift order
			GiftOrderNode.remove_order()
