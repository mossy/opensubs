extends Control

@export var AdjustMultiplier : float = 0.08

var QueuedOrderScene : PackedScene = preload("res://Scenes/InterfaceElements/QueuedOrder.tscn")

var PressedAngle : float
var Value : float
var PreviousValue : float
var PreviousCursorAngle : float

func _process(delta : float) -> void:
	update_time_label()
	$Wheel/WheelSprite.rotation_degrees = Value / AdjustMultiplier
	if $Wheel/WheelButton.button_pressed and GameVariables.Jumping == false:
		if get_cursor_angle() != PreviousCursorAngle:
			$Wheel/WheelPoint.look_at(get_global_mouse_position())
			Value = PreviousValue + ($Wheel/WheelPoint.rotation_degrees - PressedAngle) * AdjustMultiplier
			clamp_value()
			get_node("/root/Game").set_visible_tick(int(Value))

func update_tick() -> void:
	Value = GameVariables.VisibleTick

func set_queued_order(ordernode : Node) -> void:
	var Order : Dictionary = ordernode.Order
	if Order["User"] == Settings.User:
		if Order["OrderTick"] > GameVariables.GlobalTick:
			# Check that queued order does not exist
			for queuedordernode in $ScrollContainer/QueuedOrders.get_children():
				if queuedordernode.OrderNode == ordernode:
					return 
			# Set queued order instance
			var QueuedOrderSceneInstance : Node = QueuedOrderScene.instantiate()
			QueuedOrderSceneInstance.OrderNode = ordernode
			# Get position in parent
			var PositionInParent : int
			for queuedordernode in $ScrollContainer/QueuedOrders.get_children():
				# Check that order comes after child order 
				if queuedordernode.Order["OrderTick"] <= Order["OrderTick"]:
					PositionInParent = queuedordernode.get_index() + 1
				else:
					break
			# Set queued order button instance
			$ScrollContainer/QueuedOrders.add_child(QueuedOrderSceneInstance)
			$ScrollContainer/QueuedOrders.move_child(QueuedOrderSceneInstance, PositionInParent)

func update_queued_order_status(order : Dictionary) -> void:
	for queuedordernode in $ScrollContainer/QueuedOrders.get_children():
		if (
			queuedordernode.Order["OrderTick"] == order["OrderTick"] and
			queuedordernode.Order["OrderId"] == order["OrderId"]
		):
			queuedordernode.update_order_status()

func remove_past_orders() -> void:
	for queuedordernode in $ScrollContainer/QueuedOrders.get_children():
		if queuedordernode.Order["OrderTick"] <= GameVariables.GlobalTick:
			queuedordernode.queue_free()

func clamp_value() -> void:
	Value = clamp(Value, 0, GameVariables.MaximumTick - 1)

func update_time_label() -> void:
	var Time : int = (GameVariables.VisibleTick - GameVariables.GlobalTick) * GameVariables.TickLength
	if Time >= 0:
		$TimeLabel.text = VariableFunctions.get_time_text(Time)  + " from now"
	else:
		$TimeLabel.text = VariableFunctions.get_time_text(-Time)  + " ago"

func get_cursor_angle() -> float:
	return rad_to_deg($Wheel/WheelPoint.global_position.angle_to(get_global_mouse_position()))

func _on_WheelButton_button_down() -> void:
	$Wheel/WheelPoint.look_at(get_global_mouse_position())
	PressedAngle = $Wheel/WheelPoint.rotation_degrees
	PreviousValue = Value
	PreviousCursorAngle = get_cursor_angle()

func _on_WheelButton_button_up() -> void:
	var WheelCursorMovement : float = get_global_mouse_position().x - $Wheel/WheelPoint.global_position.x
	if get_cursor_angle() == PreviousCursorAngle:
		Value += WheelCursorMovement / abs(WheelCursorMovement)
		clamp_value()
		if int(Value) != GameVariables.VisibleTick:
			get_node("/root/Game").set_visible_tick(int(Value))

func _on_TimePanel_visibility_changed() -> void:
	if visible:
		Value = GameVariables.VisibleTick
