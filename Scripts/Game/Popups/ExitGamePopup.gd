extends "res://Scripts/InterfaceElements/Popup.gd"

func _on_YesButton_pressed() -> void:
	# Set game exiting
	get_node("/root/Game").Exiting = true
	# Close game
	Network.rpc_id(1, "close_game", GameVariables.GameId, multiplayer.get_unique_id(), Settings.User)
	# Reset game variables
	GameVariables.set_script(null)
	GameVariables.set_script(preload("res://Scripts/SingletonVariables/GameVariables.gd"))
	# Change scene
	get_tree().change_scene_to_file("res://Scenes/Menu/Menu.tscn")

func _on_NoButton_pressed() -> void:
	get_node("/root/Game/%ExitButton").button_pressed = false
	hide()
