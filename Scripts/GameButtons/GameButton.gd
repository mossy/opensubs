extends Control

var GameId : int

func set_game(gameid : int, gameinfo : Dictionary) -> void:
	GameId = gameid
	# Set name button text
	$NameButton.text = gameinfo["GameName"] + " - " + str(len(gameinfo["Users"])) + "/" + str(gameinfo["PlayerCount"]) + " players"

func _on_NameButton_pressed() -> void:
	get_node("/root/Menu").show_game_info(GameId)
