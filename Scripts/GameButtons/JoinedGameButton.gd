extends "res://Scripts/GameButtons/GameButton.gd"

func _on_InteractButton_pressed() -> void:
	GameVariables.GameId = GameId
	Network.rpc_id(1, "open_game", GameId, multiplayer.get_unique_id(), Settings.User)
