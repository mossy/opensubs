extends "res://Scripts/GameButtons/GameButton.gd"

func _on_InteractButton_pressed() -> void:
	# Join game
	Network.rpc_id(1, "join_game", GameId, multiplayer.get_unique_id(), Settings.User)
	# Show joined games menu panel
	get_node("/root/Menu/%JoinedGamesMenuPanel").show()
