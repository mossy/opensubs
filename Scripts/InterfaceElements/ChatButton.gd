extends Control

var Users : Array

func _process(delta : float) -> void:
	custom_minimum_size.y = $Message.size.y + 20

func set_message(message : Dictionary) -> void:
	var OtherUsers : Array = Users.duplicate(true)
	OtherUsers.erase(Settings.User)
	var OtherUsersMessage : Dictionary = message.duplicate(true)
	OtherUsersMessage["Users"] = OtherUsers
	$Message.set_message(OtherUsersMessage)

func _on_ChatButton_pressed() -> void:
	get_node("/root/Game/%CommunicationsPanel").set_chat(Users)
