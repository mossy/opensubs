extends Button

@onready var PanelSize : Vector2 = get_parent().size

var PreviousPosition : Vector2
var PressedCursorPosition : Vector2

func _process(delta : float) -> void:
	if button_pressed:
		var PanelBottom : float = get_parent().position.y + get_parent().size.y
		get_parent().position.y = min(PreviousPosition.y + get_global_mouse_position().y - PressedCursorPosition.y, PanelBottom - PanelSize.y)
		get_parent().size.y = PanelBottom - get_parent().position.y

func _on_ExpandButton_visibility_changed() -> void:
	if visible:
		var PanelBottom : float = get_parent().position.y + get_parent().size.y
		get_parent().position.y = PanelBottom - PanelSize.y
		get_parent().size.y = PanelSize.y

func _on_ExpandButton_button_down() -> void:
	PreviousPosition = get_parent().position
	PressedCursorPosition = get_global_mouse_position()
