extends HBoxContainer

@export var Suffix : String

func set_value(value) -> void:
	var ValueString : String = str(value)
	if typeof(value) == TYPE_STRING and name != "GameName":
		ValueString = value.capitalize()
	$ValueLabel.text = ValueString + Suffix
