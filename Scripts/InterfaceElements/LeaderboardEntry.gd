extends Control

# Player variables

var PlayerNode : Node
var PlayerHistory : Array
var Player : String

# Create player order variables

var CreatePlayerOrder : Dictionary

# Inbuilt functions

func _ready() -> void:
	# Set player variables
	PlayerHistory = PlayerNode.History
	Player = PlayerNode.name
	# Set create player order variables
	CreatePlayerOrder = PlayerNode.CreateItemOrder
	# Set interface elements
	var PlayerColor : Color = Color(GameVariables.Colors[Player])
	theme = theme.duplicate(true)
	theme.get_stylebox("fill", "ProgressBar").bg_color = PlayerColor
	theme.get_stylebox("background", "ProgressBar").bg_color = PlayerColor.lightened(0.5)
	$User/OfflineIndicator.modulate = PlayerColor.lightened(0.5)

# Tick functions

func update_tick() -> void:
	# User elements
	$User/Label.text = CreatePlayerOrder["User"]
	# Get player history entries
	var FutureTickAhead : int = min(GameVariables.MaximumTick - GameVariables.VisibleTick, 100)
	var FutureTick : int = GameVariables.VisibleTick + FutureTickAhead
	var PlayerHistoryEntry : Dictionary = PlayerNode.get_history_entry(GameVariables.VisibleTick)
	var FuturePlayerHistoryEntry : Dictionary = PlayerNode.get_history_entry(FutureTick)
	# Set info elements
	$WinningStripe.hide()
	var InfoType : String = get_node("/root/Game/%LeaderboardPanel").InfoType
	match InfoType:
		"Troops":
			# Get greatest player charge
			# TODO: move to panel
			var GreatestPlayerCharge : int = 1
			for chargeplayernode in get_node("/root/Game/Items/Players").get_children():
				# Get charge player data
				var ChargePlayerHistoryEntry : Dictionary = chargeplayernode.get_history_entry(GameVariables.VisibleTick)
				# Set greatest player charge
				if ChargePlayerHistoryEntry["ChargeTotal"] > GreatestPlayerCharge:
					GreatestPlayerCharge = ChargePlayerHistoryEntry["ChargeTotal"]
			# Set progress bar
			$ProgressBar.size.x = float(PlayerHistoryEntry["ChargeTotal"]) / float(GreatestPlayerCharge) * size.x
			$ProgressBar.max_value = PlayerHistoryEntry["ChargeTotal"]
			$ProgressBar.value = PlayerHistoryEntry["TroopsTotal"]
			# Set troops label
			var FutureTroopsIncrease : int = FuturePlayerHistoryEntry["TroopsTotal"] - PlayerHistoryEntry["TroopsTotal"]
			$Info/Troops/Label.text = (
				str($ProgressBar.value) + " of " + str($ProgressBar.max_value) +
				" (+" + str(FutureTroopsIncrease) + " across " + str(FutureTickAhead) + " ticks)"
			)
		"Outposts":
			# Set progress bar
			$ProgressBar.size.x = size.x
			if GameVariables.VictoryType == "Domination":
				$WinningStripe.show()
				$ProgressBar.max_value = GameVariables.DominationVictoryTotal
			else:
				$ProgressBar.max_value = GameVariables.OutpostsTotal
			$ProgressBar.value = PlayerHistoryEntry["OutpostsTotal"]
			# Get outpost counts
			var PlayerOutpostsTypesTotals : Dictionary
			for outpostnode in get_node("/root/Game/Items/Outposts").get_children():
				# Get outpost data
				var OutpostHistoryEntry : Dictionary = outpostnode.get_history_entry(GameVariables.VisibleTick)
				# Check that outpost player is player
				if OutpostHistoryEntry["CurrentPlayer"] == Player:
					var OutpostType : String = OutpostHistoryEntry["CurrentType"]
					if PlayerOutpostsTypesTotals.has(OutpostType) == false:
						PlayerOutpostsTypesTotals[OutpostType] = 0
					PlayerOutpostsTypesTotals[OutpostType] += 1
			# Set outpost types labels
			for child in $Info/Outposts.get_children():
				child.get_node("Label").text = "x" + str(PlayerOutpostsTypesTotals.get(child.name, 0))
		"Ore":
			# Set progress bar
			$ProgressBar.size.x = size.x
			if GameVariables.VictoryType == "Mining":
				$WinningStripe.show()
				$ProgressBar.max_value = GameVariables.MiningVictoryTotal
			else:
				$ProgressBar.max_value = 0
			$ProgressBar.value = PlayerHistoryEntry["OreTotal"]
			# Set ore label
			var FutureOreIncrease : int = FuturePlayerHistoryEntry["OreTotal"] - PlayerHistoryEntry["OreTotal"]
			$Info/Ore/Label.text = (
				str($ProgressBar.value) + " of " + str($ProgressBar.max_value) +
				" (+" + str(FutureOreIncrease) + " across " + str(FutureTickAhead) + " ticks)"
			)
	# Set player info visible
	for child in $Info.get_children():
		child.visible = child.name == InfoType
	# Set online indicator
	var UserOnline = GameVariables.OnlineUsers.has(CreatePlayerOrder["User"])
	$User/OnlineIndicator.visible = UserOnline
	$User/OfflineIndicator.visible = UserOnline == false
