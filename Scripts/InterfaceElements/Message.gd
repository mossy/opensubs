extends VBoxContainer

var UserPillScene : PackedScene = preload("res://Scenes/InterfaceElements/UserPill.tscn")

func set_message(message : Dictionary) -> void:
	for user in message["Users"]:
		var UserPillInstance : Node = UserPillScene.instantiate()
		UserPillInstance.User = user
		UserPillInstance.disabled = true
		$Header/HBoxContainer.add_child(UserPillInstance)
	var TimeText : String = str(VariableFunctions.get_time_text(Time.get_unix_time_from_system() - message["Time"], true, 1))
	if TimeText != "Now":
		TimeText += " ago"
	$Header/TimeLabel.text = TimeText
	$TextLabel.text = message["Text"]
