extends Control

var OrderNode : Node
var Order : Dictionary

func _ready() -> void:
	# Set order variables
	Order = OrderNode.Order
	# Connect order queue free to self queue free
	OrderNode.connect("tree_exiting", queue_free)

func _process(delta : float) -> void:
	# Set time to order text
	var TimeToOrder : int = OrderNode.get_time_to_order()
	var TimeToOrderText : String = VariableFunctions.get_time_text(TimeToOrder)
	var ButtonText : String = get_order_action_name() + " (" + TimeToOrderText + ")"
	# Set button elements
	for child in get_children():
		child.text = ButtonText

# Update order invalid or warning status
func update_order_status() -> void:
	var VisibleOrderButton : Node = $ValidOrderButton
	# Check if order is invalid
	if OrderNode.InvalidReasons.is_empty() == false:
		VisibleOrderButton = $InvalidOrderButton
	# Check if order is warning
	elif OrderNode.WarningReasons.is_empty() == false:
		VisibleOrderButton = $WarningOrderButton
	# Set button visibility
	for child in get_children():
		child.visible = child == VisibleOrderButton

func get_order_action_name() -> String:
	match Order["Type"]:
		# Outpost orders
		"TransformOutpost":
			match Order["TransformType"]:
				"Mine":
					return "Mine"
		# Submarine orders
		"CreateSubmarine":
			return "Launch"
		"GiftSubmarine":
			return "Gift"
	return ""

func _on_QueuedOrder_visibility_changed() -> void:
	if visible:
		_ready()
		# Set order icons
		for child in get_children():
			for secondarychild in child.get_node("OrderIcons").get_children():
				secondarychild.visible = secondarychild.name == get_order_action_name()
		# Update order status
		update_order_status()

func _on_OrderButton_pressed() -> void:
	# Set vbisible tick to order tick
	get_node("/root/Game").set_visible_tick(Order["OrderTick"])
	# Focus order
	OrderNode.ItemNode.select()

func _on_CancelButton_pressed() -> void:
	OrderNode.remove_order()
