extends Button

signal value_changed (value)

@export var MaximumValue : int
@export var MinimumValue : float
@export var InitialValue : float
@export var Step : float = 1
@export var Suffix : String

@onready var ValueTween : Tween

var ScrollWheelNotchScene : PackedScene = preload("res://Scenes/InterfaceElements/ScrollWheelNotch.tscn")

var Value : float
var StepValue : float
var PreviousStepValue : float
var NotchTotal : int = 15
var AdjustMultiplier : float = 0.05
var VerticalMargin : int = 2
var PressedMapCursorPosition : Vector2
var PressedValue : float

func _ready() -> void:
	set_value(InitialValue)
	update_number_label()
	for i in NotchTotal:
		# Set scroll wheel notch instance
		var ScrollWheelNotchInstance : Node = ScrollWheelNotchScene.instantiate()
		add_child(ScrollWheelNotchInstance)

func _process(delta : float) -> void:
	if button_pressed:
		Value = clamp(PressedValue + (get_global_mouse_position().x - PressedMapCursorPosition.x) * AdjustMultiplier * Step, MinimumValue, MaximumValue)
		StepValue = snapped(Value, Step)
		if StepValue != PreviousStepValue:
			emit_signal("value_changed", StepValue)
			update_number_label()
		PreviousStepValue = StepValue
	if button_pressed or (ValueTween != null and ValueTween.is_running()):
		update_notch_positions()

func set_value(value : float) -> void:
	Value = clamp(value, MinimumValue, MaximumValue)
	StepValue = snapped(Value, Step)

func update_number_label() -> void:
	var NumberLabel : Node = get_node_or_null("../NumberLabel")
	if NumberLabel != null:
		NumberLabel.text = str(StepValue) + Suffix

func update_notch_positions() -> void:
	for i in get_child_count():
		var CircumferencePosition : Vector2 = Vector2(1, 0) + Vector2.RIGHT.rotated(deg_to_rad(360.0 / get_child_count() * i - (Value - InitialValue) / AdjustMultiplier / Step))
		var XPosition : float = CircumferencePosition.x * size.x / 2
		get_child(i).points = [Vector2(XPosition, VerticalMargin), Vector2(XPosition, size.y - VerticalMargin * 2)]
		get_child(i).visible = CircumferencePosition.y >= 0

func _on_button_down() -> void:
	PressedValue = Value
	PressedMapCursorPosition = get_global_mouse_position()

func _on_button_up() -> void:
	ValueTween = get_tree().create_tween()
	ValueTween.tween_property(self, "Value", StepValue, 0.25)

func _on_draw() -> void:
	update_notch_positions()
