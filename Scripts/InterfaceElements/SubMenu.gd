extends Control

func _on_visibility_changed() -> void:
	if visible:
		custom_minimum_size.y = 0
		for childnode in $Panel/VBoxContainer.get_children():
			if childnode.visible:
				custom_minimum_size.y += (10 + 50)
