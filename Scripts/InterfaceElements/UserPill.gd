extends Button

var User : String
var Player : String
var UpdateMessages : bool = true

func _ready() -> void:
	# Set variables
	Player = get_node("/root/Game").get_user_player(User)
	# Set elements
	text = User
	custom_minimum_size.x = theme.get_font("label", "Font").get_string_size(User).x + 10
	theme = theme.duplicate(true)
	# Call toggled
	UpdateMessages = false
	_on_toggled(button_pressed)
	UpdateMessages = true

func _on_toggled(buttonpressed : bool) -> void:
	# Get current users
	var CurrentUsers : Array = get_node("/root/Game/%CommunicationsPanel").CurrentUsers
	# Set theme
	var PlayerColor : Color = Color(GameVariables.Colors[Player])
	if buttonpressed == false and disabled == false:
		PlayerColor = PlayerColor.lightened(0.25)
	for stylebox in theme.get_stylebox_list("Button"):
		theme.get_stylebox(stylebox, "Button").bg_color = PlayerColor
	# Update messages
	if UpdateMessages:
		# Set current users
		if buttonpressed:
			CurrentUsers.append(User)
		else:
			CurrentUsers.erase(User)
		# Update current users
		CurrentUsers.sort()
		get_node("/root/Game/%CommunicationsPanel").update_messages(CurrentUsers)
