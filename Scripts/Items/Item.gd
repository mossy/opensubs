extends Node2D

# Order variables

var CreateItemOrderNode : Node
var CreateItemOrder : Dictionary
var ItemOrderNodes : Dictionary

# Item variables

var Active : bool
var Data : Dictionary
var History : Array
var GlobalHistoryEntry : Dictionary
var VisibleHistoryEntry : Dictionary

# Inbuilt functions

func _ready() -> void:
	# Set create item order variables
	CreateItemOrder = CreateItemOrderNode.Order
	# Connect create item order queue free to self queue free
	CreateItemOrderNode.connect("tree_exiting", queue_free)
	# Calculate data
	calculate_data()
	# Set active
	if GameVariables.AllActive:
		set_active(CreateItemOrder["OrderTick"])

# Data functions

func calculate_data() -> void:
	return

# History entry functions

# Returns the index in history for passed tick
func get_history_index(tick : int) -> int:
	var HistoryIndex : int = tick - History[0]["Tick"]
	if HistoryIndex >= 0 and HistoryIndex < len(History):
		return HistoryIndex
	else:
		# Failure case
		return -1

# Returns the entry in history for passed tick
func get_history_entry(tick : int) -> Dictionary:
	var HistoryIndex : int = get_history_index(tick)
	if HistoryIndex != -1:
		return History[HistoryIndex]
	else:
		# Failure case
		return {}

func calculate_history_expansion(tick : int) -> void:
	# Get data
	var HistoryEntry : Dictionary = get_history_entry(tick)
	var EffectedOutposts : Array = HistoryEntry.get("EffectedOutposts", []).duplicate(true)
	var EffectedSubmarines : Array = HistoryEntry.get("EffectedSubmarines", []).duplicate(true)
	# Set history previous entry
	set_history_current_entry_previous_entry(tick)
	# Set initial values
	set_initial_values(tick)
	# Calculate position
	calculate_position(tick)
	# Calculate value generation
	calculate_value_generation(tick)
	# Get effected outposts active
	for effectedoutpost in EffectedOutposts:
		var EffectedOutpostNode : Node = get_node("/root/Game/Items/Outposts/" + effectedoutpost)
		EffectedOutpostNode.set_active(tick)
	# Get effected submarines active
	for effectedsubmarine in EffectedSubmarines:
		var EffectedSubmarineNode : Node = get_node("/root/Game/Items/Submarines/" + effectedsubmarine)
		EffectedSubmarineNode.set_active(tick)
	# TODO: remove
	# Check if item is a submarine
	if Data["Type"] == "Submarine":
		# Check that effected items are not empty
		if (EffectedOutposts + EffectedSubmarines).is_empty() == false:
			# Check that effected items are not just initial outpost
			if (
				(EffectedOutposts + EffectedSubmarines) == [CreateItemOrder["InitialOutpost"]] and
				tick == CreateItemOrder["OrderTick"]
			) == false:
				# Set player active
				var PlayerNode : Node = get_node("/root/Game/Items/Players/" + HistoryEntry["CurrentPlayer"])
				PlayerNode.set_active(tick)

# Sets history entry to history entry at previous tick
func set_history_current_entry_previous_entry(tick : int) -> void:
	# Get data
	var PreviousHistoryEntry : Dictionary = get_previous_history_entry(tick)
	# Get history entry
	var NewHistoryEntry : Dictionary = PreviousHistoryEntry.duplicate(true)
	NewHistoryEntry["Tick"] = tick
	# TODO: remove
	# Reset history values
	NewHistoryEntry["EffectedOutposts"] = []
	NewHistoryEntry["EffectedSubmarines"] = []
	if Data["Type"] == "Submarine":
		NewHistoryEntry["CurrentDetector"] = ""
	# Set history entry
	var CurrentHistoryIndex : int = get_history_index(tick)
	if CurrentHistoryIndex != -1:
		History.remove_at(CurrentHistoryIndex)
		History.insert(CurrentHistoryIndex, NewHistoryEntry)
	else:
		History.append(NewHistoryEntry)

# Set initial create order values
func set_initial_values(tick : int) -> void:
	return

func calculate_position(tick : int) -> void:
	return

func calculate_value_generation(tick : int) -> void:
	return

func calculate_history_effect(tick : int) -> void:
	var PreviousOrderNodes : Array = get_previous_item_order_nodes(tick)
	# Update order nodes
	for ordernode in PreviousOrderNodes:
		# Set invalid reasons
		ordernode.set_invalid_reasons(tick)
		# Calculates order effect
		ordernode.calculate_effect(tick)
		# Updates order in queued orders panel
		get_node("/root/Game/%TimePanel").update_queued_order_status(ordernode.Order)
	# Update order nodes
	for ordernode in PreviousOrderNodes:
		# Set warning reasons
		ordernode.set_warning_reasons(tick)

# Gets previous history entry
func get_previous_history_entry(tick : int) -> Dictionary:
	# Get data
	var DefaultHistoryEntry : Dictionary = Data["DefaultHistoryEntry"]
	# Get history entry
	var PreviousHistoryEntry : Dictionary = get_history_entry(tick - 1)
	if PreviousHistoryEntry.is_empty() == false:
		return PreviousHistoryEntry
	else:
		return DefaultHistoryEntry

# History total functions

# Set history total
func set_history_total(tick : int, total : String, value : int) -> void:
	# Check that value will make a difference
	if value != 0:
		# Get data
		var HistoryEntry : Dictionary = get_history_entry(tick)
		# Set history
		HistoryEntry[total] += value

# Set history shield
func set_history_shield(tick : int, value : int) -> void:
	set_history_total(tick, "ShieldTotal", value)

# Set history troops
func set_history_troops(tick : int, value : int, updaterelatedvariables : bool = true) -> void:
	if updaterelatedvariables:
		update_history_troops_related_variables(tick, value)
	set_history_total(tick, "TroopsTotal", value)

# Set history ore
func set_history_ore(tick : int, value : int) -> void:
	set_history_total(tick, "OreTotal", value)

# Set history charge
func set_history_charge(tick : int, value : int) -> void:
	set_history_total(tick, "ChargeTotal", value)

func set_history_outposts(tick : int, value : int) -> void:
	set_history_total(tick, "OutpostsTotal", value)

# History current functions

# Set history current
func set_history_current(tick : int, current : String, value) -> void:
	# Get data
	var HistoryEntry : Dictionary = get_history_entry(tick)
	# Set history
	HistoryEntry[current] = value

# Set history player
func set_history_player(tick : int, value : String, updaterelatedvariables : bool = true) -> void:
	if updaterelatedvariables:
		update_history_player_related_variables(tick, value)
	set_history_current(tick, "CurrentPlayer", value)

# Set history detector
func set_history_detector(tick : int, value : String) -> void:
	set_history_current(tick, "CurrentDetector", value)

# Set history type
func set_history_type(tick : int, value : String) -> void:
	set_history_current(tick, "CurrentType", value)

# Set history target
func set_history_target(tick : int, value : String) -> void:
	set_history_current(tick, "CurrentTarget", value)

# Update related variables functions

# Update history troops related variables
func update_history_troops_related_variables(tick : int, value : int) -> void:
	if value != 0:
		# Get data
		var HistoryEntry : Dictionary = get_history_entry(tick)
		# Get player
		var Player : String = HistoryEntry.get("CurrentPlayer", "")
		if Player != "Dormant" and Player != "":
			var PlayerNode : Node = get_node("/root/Game/Items/Players/" + Player)
			# Set player history
			PlayerNode.set_active(tick)
			PlayerNode.set_history_troops(tick, value, false)

# Update history player related variables
func update_history_player_related_variables(tick : int, value : String) -> void:
	# Get data
	var HistoryEntry : Dictionary = get_history_entry(tick)
	# Get players
	var PreviousPlayer : String = HistoryEntry["CurrentPlayer"]
	var CurrentPlayer : String = value
	# Get player charge
	var Charge : int
	if Data["Type"] == "Outpost" and HistoryEntry["CurrentType"] == "Generator":
		Charge = GameVariables.DefaultChargePerGenerator
	# Set player troops
	if PreviousPlayer != "Dormant" and PreviousPlayer != "":
		var PreviousPlayerNode : Node = get_node("/root/Game/Items/Players/" + PreviousPlayer)
		PreviousPlayerNode.set_active(tick)
		PreviousPlayerNode.set_history_troops(tick, -HistoryEntry["TroopsTotal"], false)
		if Data["Type"] == "Outpost":
			PreviousPlayerNode.set_history_charge(tick, -Charge)
			PreviousPlayerNode.set_history_outposts(tick, -1)
	if CurrentPlayer != "Dormant":
		var CurrentPlayerNode : Node = get_node("/root/Game/Items/Players/" + CurrentPlayer)
		CurrentPlayerNode.set_active(tick)
		CurrentPlayerNode.set_history_troops(tick, HistoryEntry["TroopsTotal"], false)
		if Data["Type"] == "Outpost":
			CurrentPlayerNode.set_history_charge(tick, Charge)
			CurrentPlayerNode.set_history_outposts(tick, 1)

# Active functions

func set_active(tick : int) -> void:
	if (
		History.is_empty() or
		(History[0]["Tick"] <= tick and History[-1]["Tick"] + 1 >= tick)
	):
		if Active == false:
			# Set active true
			Active = true
			# Check if all items are active
			if GameVariables.AllActive == false:
				print("Set active " + get_parent().name + " " + name + " at " + str(tick))
				# Set related items active
				set_related_items_active(tick)
				# Calculate history expansion
				calculate_history_expansion(tick)
			else:
				# Calculate history expansion
				calculate_history_expansion(tick)
		# TODO IMPORTANT: sort

func remove_active(tick : int) -> void:
	if Active == true:
		# Set active false
		Active = false
		# Check if all items are active
		if GameVariables.AllActive == false:
			print("Remove active " + get_parent().name + " " + name + " at " + str(tick))
	# TODO IMPORTANT: sort

func set_related_items_active(tick : int) -> void:
	return

# Tick functions

# Update visuals to visible tick
func update_tick() -> void:
	return

# Order functions

func get_previous_item_order_nodes(currentordertick : int, ordertype : String = "", notempty = false) -> Array:
	var PreviousItemOrderNodes : Array
	for ordertick in ItemOrderNodes:
		for ordernode in ItemOrderNodes[ordertick]:
			# Check that order type is passed order type
			var Order : Dictionary = ordernode.Order
			if ordertype == Order["Type"] or ordertype == "":
				PreviousItemOrderNodes.append(ordernode)
	if PreviousItemOrderNodes.is_empty() and notempty:
		PreviousItemOrderNodes.append(Node.new())
	return PreviousItemOrderNodes
