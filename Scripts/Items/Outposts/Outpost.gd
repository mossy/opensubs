extends "res://Scripts/Items/Item.gd"

# Data functions

# Calculate data
func calculate_data() -> void:
	# Get outpost default history entry
	var DefaultHistoryEntry : Dictionary = {
		"Tick" : CreateItemOrder["OrderTick"],
		# Status variables
		"Position" : CreateItemOrder["InitialPosition"],
		# Countdown variables
		"ShieldTicksCountdown" : GameVariables.GenerateShieldMaximumTicks / CreateItemOrder["ShieldMaximum"],
		"TroopsTicksCountdown" : GameVariables.GenerateTroopsTicks,
		"OreTicksCountdown" : GameVariables.GenerateOreTicks,
		# Total variables
		"TroopsTotal" : 0,
		"ShieldTotal" : 0,
		"SonarRangeMultipliersTotal" : 0,
		"ShieldMaximumMultipliersTotal" : 0,
		"TroopsTicksMultipliersTotal" : 0,
		"TroopsNumberMultipliersTotal" : 0,
		"OreTicksMultipliersTotal" : 0,
		"OreNumberMultipliersTotal" : 0,
		# Current variables
		"CurrentPlayer" : "",
		"CurrentDetector" : "",
		"CurrentType" : "",
		# Effected lists
		"EffectedOutposts" : [],
		"EffectedSubmarines" : []
	}
	# Set outpost variables
	Data = {
		# Info
		"Name" : name,
		"Type" : "Outpost",
		"History" : [DefaultHistoryEntry.duplicate(true)],
		"DefaultHistoryEntry" : DefaultHistoryEntry,
		# Outpost order variables
		"CreateOutpostOrderTick" : CreateItemOrder["OrderTick"],
		"CreateOutpostOrderId" : CreateItemOrder["OrderId"],
	}
	History = Data["History"]

# History functions

# Set initial create order values
func set_initial_values(tick : int) -> void:
	# Check if tick is create order tick
	if tick == CreateItemOrder["OrderTick"]:
		# Set history values
		set_history_type(tick, CreateItemOrder["InitialType"])
		set_history_player(tick, CreateItemOrder["InitialPlayer"])
		set_history_troops(tick, CreateItemOrder["InitialTroops"])

# Generation functions

func get_generate_shield_ticks(tick : int) -> int:
	var HistoryEntry : Dictionary = get_history_entry(tick)
	return int(ceil(GameVariables.GenerateShieldMaximumTicks / CreateItemOrder["ShieldMaximum"] / ( 1 + HistoryEntry["ShieldMaximumMultipliersTotal"])))

func get_generate_troops_ticks(tick : int) -> int:
	var HistoryEntry : Dictionary = get_history_entry(tick)
	return int(ceil(GameVariables.GenerateTroopsTicks * (1 + HistoryEntry["TroopsTicksMultipliersTotal"])))

func get_generate_ore_ticks(tick : int) -> int:
	var HistoryEntry : Dictionary = get_history_entry(tick)
	return int(ceil(GameVariables.GenerateOreTicks * (1 + HistoryEntry["OreTicksMultipliersTotal"])))

# Calculate shield and troop generation for passed tick
func calculate_value_generation(tick : int) -> void:
	# Get history entries
	var HistoryEntry : Dictionary = get_history_entry(tick)
	# Check that outpost is not dormant
	var Player : String = HistoryEntry["CurrentPlayer"]
	if Player != "Dormant":
		# Get player info
		var PlayerNode : Node = get_node("/root/Game/Items/Players/" + Player)
		var PlayerHistoryEntry : Dictionary = PlayerNode.get_history_entry(tick)
		# Check that outpost is factory
		if HistoryEntry["CurrentType"] == "Factory":
			# Set troops ticks countdown
			HistoryEntry["TroopsTicksCountdown"] -= 1
			HistoryEntry["TroopsTicksCountdown"] += int(HistoryEntry["TroopsTicksCountdown"] < 0) * get_generate_troops_ticks(tick)
			# Set troops increase
			if HistoryEntry["TroopsTicksCountdown"] == 0:
				var TroopsIncrease : int = ceil(GameVariables.GenerateTroopsNumber * (1 + HistoryEntry["TroopsNumberMultipliersTotal"]))
				var TroopsIncreaseMaximum : int = max(PlayerHistoryEntry["ChargeTotal"] - PlayerHistoryEntry["TroopsTotal"], 0)
				set_history_troops(tick, min(TroopsIncrease, TroopsIncreaseMaximum))
		# Check that outpost is mine
		if HistoryEntry["CurrentType"] == "Mine":
			# Set ore ticks countdown
			HistoryEntry["OreTicksCountdown"] -= 1
			HistoryEntry["OreTicksCountdown"] += int(HistoryEntry["OreTicksCountdown"] < 0) * get_generate_ore_ticks(tick)
			# Set ore increase
			if HistoryEntry["OreTicksCountdown"] == 0:
				var OreIncrease : int = ceil(1 * (1 + HistoryEntry["OreNumberMultipliersTotal"])) * PlayerHistoryEntry["OutpostsTotal"]
				var OreIncreaseMaximum : int = max(GameVariables.MiningVictoryTotal - PlayerHistoryEntry["OreTotal"], 0)
				PlayerNode.set_history_ore(tick, min(OreIncrease, OreIncreaseMaximum))
		# Set shield ticks countdown
		HistoryEntry["ShieldTicksCountdown"] -= 1
		HistoryEntry["ShieldTicksCountdown"] += int(HistoryEntry["ShieldTicksCountdown"] < 0) * get_generate_shield_ticks(tick)
		# Set shield increase
		if HistoryEntry["ShieldTicksCountdown"] == 0:
			var ShieldIncrease : int = 1
			var ShieldIncreaseMaximum : int = CreateItemOrder["ShieldMaximum"] - HistoryEntry["ShieldTotal"]
			set_history_shield(tick, min(ShieldIncrease, ShieldIncreaseMaximum))

# Sonar functions

# Calculate obscured item visible by outpost
func calculate_obscured_item_visible(tick : int, obscureditemnode : Node) -> void:
	# Get obscured item data
	var ObscuredItemHistory : Array = obscureditemnode.History
	# Check that obscured item is present at tick
	if ObscuredItemHistory[0]["Tick"] <= tick and ObscuredItemHistory[-1]["Tick"] >= tick:
		# Get obscured item data
		var ObscuredItemHistoryEntry : Dictionary = obscureditemnode.get_history_entry(tick)
		var ObscuredItemPeviousHistoryEntry : Dictionary = obscureditemnode.get_previous_history_entry(tick)
		# Check that obscured item was not previously obscured
		if (
			tick <= GameVariables.GlobalTick or
			tick == ObscuredItemHistory[0]["Tick"] or
			ObscuredItemPeviousHistoryEntry["CurrentDetector"] != ""
		):
			# Check that obscured item is not already visible
			if ObscuredItemHistoryEntry["CurrentDetector"] == "":
				# Get data
				var HistoryEntry : Dictionary = get_history_entry(tick)
				# Get obscured item visible
				var Position : Vector2 = HistoryEntry["Position"]
				var ObscuredItemPosition : Vector2 = ObscuredItemHistoryEntry["Position"]
				var ObscuredItemWrappedPosition : Vector2 = get_node("/root/Game").get_closest_wrapped_position(Position, ObscuredItemPosition)
				var ObscuredItemDistance : float = Position.distance_to(ObscuredItemWrappedPosition)
				# Set obscured item visible
				if ObscuredItemDistance <= get_sonar_range(tick):
					# TODO IMPORTANT: reconsider
					# Set obscured item active
					obscureditemnode.set_active(tick)
					# Set sonar item in obscured item as visible
					obscureditemnode.set_history_detector(tick, name)

func update_sonar(tick : int) -> void:
	print("Updating Outposts " + name + " sonar at " + str(tick))
	# Remove history detector
	set_history_detector(tick, "")
	# Get item nodes with outpost first
	var OutpostNodes : Array = get_node("/root/Game/Items/Outposts").get_children()
	OutpostNodes.erase(self)
	OutpostNodes = [self] + OutpostNodes
	var SumbarineNodes : Array = get_node("/root/Game/Items/Submarines").get_children()
	# Set outposts sonar
	var SonarStatus : bool = get_sonar_status(tick)
	for outpostnode in OutpostNodes:
		# Calculate outpost visible from other outpost
		outpostnode.calculate_obscured_item_visible(tick, self)
		# Check outpost sonar
		if SonarStatus:
			# Calculate other outpost visible from outpost
			calculate_obscured_item_visible(tick, outpostnode)
		else:
			# Update other outpost sonar
			var OutpostHistoryEntry : Dictionary = outpostnode.get_history_entry(tick)
			if OutpostHistoryEntry["CurrentDetector"] == name:
				outpostnode.update_sonar(tick)
	# TODO: consider rework
	for submarinenode in SumbarineNodes:
		# Check outpost sonar
		if SonarStatus:
			# Calculate submarine visible from outpost
			calculate_obscured_item_visible(tick, submarinenode)

# Returns sonar status
func get_sonar_status(tick : int) -> bool:
	# Get data
	var HistoryEntry : Dictionary = get_history_entry(tick)
	var MiniumHistoryEntry : Dictionary = get_history_entry(min(tick, GameVariables.GlobalTick))
	# Get sonar status
	return (
		HistoryEntry["CurrentPlayer"] == GameVariables.Player and
		MiniumHistoryEntry["CurrentPlayer"] == GameVariables.Player
	)

# Returns sonar range
func get_sonar_range(tick : int) -> float:
	# Get data
	var HistoryEntry : Dictionary = get_history_entry(tick)
	# Get sonar range
	var SonarRange : float = -1
	if get_sonar_status(tick):
		SonarRange = GameVariables.DefaultSonarRange * GameVariables.SonarRangeMultiplier * (1 + HistoryEntry["SonarRangeMultipliersTotal"])
	return SonarRange

# Active functions

# Set related items active
func set_related_items_active(tick : int) -> void:
	# Get data
	var HistoryEntry : Dictionary = get_history_entry(tick)
	var PlayerNode : Node = get_node("/root/Game/Items/Players/" + HistoryEntry["CurrentPlayer"])
	# Set player active
	if PlayerNode != null:
		PlayerNode.set_active(tick)
