extends "res://Scripts/Items/Outposts/Outpost.gd"

@onready var SonarRangeMasksScene : PackedScene = preload("res://Scenes/Items/Outposts/SonarRangeMasks.tscn")

@export var SelectedSizeIncrease : int = 1.1
@export var CursorMovementAllowance : float = 10.0

var SonarRangeMasksNode : Node

var Selected : bool = false
var Focused : bool = false
var PreviousCursorPosition : Vector2
var ShowTemporaryTroops : bool = false

# Inbuilt functions

func _ready() -> void:
	super()
	# Set visuals
	$Body/Shield/ShieldRings.set_material(Shaders.ShieldRing.duplicate())
	$Body/Labels/NameLabel.text = str(name).to_upper()
	$BlockMoveAnimation.play("BlockMove")
	# Set sonar range masks
	SonarRangeMasksNode = SonarRangeMasksScene.instantiate()
	SonarRangeMasksNode.OutpostNode = self
	get_node("/root/Game/%Masks").add_child(SonarRangeMasksNode)

func _process(delta : float) -> void:
	if $Body/Button.button_pressed:
		if get_node("/root/Game").HoveredStartOutpost != self:
			var CursorMovementDifference : float = PreviousCursorPosition.distance_to(get_global_mouse_position())
			if CursorMovementDifference >= CursorMovementAllowance:
				get_node("/root/Game").set_hovered_start_outpost(self)

# Tick functions

func update_tick() -> void:
	# Get outpost data
	var HistoryEntry : Dictionary = get_history_entry(GameVariables.VisibleTick)
	# Updates position
	update_position()
	# Set outpost sprite modulate
	for child in $Body/Textures.get_children():
		child.get_node("Outpost").modulate = Color(GameVariables.Colors[HistoryEntry["CurrentPlayer"]])
	# Set transform flash animation
	var TransformOutpostOrderNode : Node = get_previous_item_order_nodes(GameVariables.VisibleTick, "TransformOutpost", true)[-1]
	if TransformOutpostOrderNode.is_inside_tree():
		# Check that gift order is within wait period
		if TransformOutpostOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick):
			$TransformFlashAnimation.play("TransformFlash")
		else:
			$TransformFlashAnimation.stop()
	else:
		$TransformFlashAnimation.stop()
	# Set outpost body
	if $TransformFlashAnimation.is_playing() == false:
		set_current_type_textures()
	# Update sonar range masks
	SonarRangeMasksNode.update_masks()
	# Set outpost details
	if HistoryEntry["CurrentDetector"] != "":
		if ShowTemporaryTroops == false:
			$Body/Labels/TroopsLabel.text = str(HistoryEntry["TroopsTotal"])
		$Body/Labels/TroopsLabel.show()
		$Body/Shield.show()
		update_shield_rings()
	else:
		$Body/Labels/TroopsLabel.hide()
		$Body/Shield.hide()
	# Set outpost panels
	set_panels()

# Visual functions

# Sets the outpost's current position
func update_position() -> void:
	# Get submarine data
	var HistoryEntry : Dictionary = get_history_entry(GameVariables.VisibleTick)
	# Check that history entry is not empty
	if HistoryEntry.is_empty() == false:
		# Set position
		var WrappedPosition : Vector2 = get_node("/root/Game").get_wrapped_position(HistoryEntry["Position"])
		global_position = WrappedPosition
		SonarRangeMasksNode.position = WrappedPosition

# Set outpost panels visible
func set_panels() -> void:
	if Selected:
		# Get outpost data
		var HistoryEntry : Dictionary = get_history_entry(GameVariables.VisibleTick)
		# Show outpost panel
		if HistoryEntry["CurrentDetector"] != "":
			get_node("/root/Game").show_outpost_panels([HistoryEntry["CurrentType"] + "Panel"], self)
		else:
			get_node("/root/Game").show_outpost_panels(["ObscuredOutpostPanel"], self)

# Focus outpost
func focus() -> void:
	if Focused == false:
		$FocusAnimation.play("Focus")
	Focused = true

func unfocus() -> void:
	if Focused:
		$FocusAnimation.play("Unfocus")
	Focused = false

func select(movecamera : bool = false) -> void:
	if History[0]["Tick"] <= GameVariables.VisibleTick and History[-1]["Tick"] >= GameVariables.VisibleTick:
		# Set selected true
		Selected = true
		# Unselect items
		get_node("/root/Game").unselect_outposts(name)
		get_node("/root/Game").unselect_submarines()
		# Set camera position to outpost position
		if movecamera:
			get_node("/root/Game").set_camera_position(global_position)
		# Set panels outpost to outpost
		var OutpostPanels : Node = get_node("/root/Game/Interface/Panels/Outposts")
		for panel in OutpostPanels.get_children():
			panel.Outpost = name
		# Set outpost panels
		set_panels()

func deselect() -> void:
	# Set selected false
	Selected = false

func set_current_type_textures() -> void:
	# Get outpost data
	var HistoryEntry : Dictionary = get_history_entry(GameVariables.VisibleTick)
	# Set outpost textures visible
	if HistoryEntry["CurrentDetector"] != "":
		for child in $Body/Textures.get_children():
			child.visible = child.name == HistoryEntry["CurrentType"]
	else:
		for child in $Body/Textures.get_children():
			child.visible = child.name == "ObscuredOutpost"

func set_transform_type_textures() -> void:
	# Get transform outpost order
	var TransformOutpostOrderNode : Node = get_previous_item_order_nodes(GameVariables.VisibleTick, "TransformOutpost", true)[-1]
	var TransformOutpostOrder : Dictionary = TransformOutpostOrderNode.Order
	# Set outpost textures visible
	for child in $Body/Textures.get_children():
		child.visible = child.name == TransformOutpostOrder["TransformType"]

# Temporarily change troops
func set_temporary_troops(troops : int) -> void:
	ShowTemporaryTroops = true
	$Body/Labels/TroopsLabel.text = str(troops)

func override_temporary_troops() -> void:
	ShowTemporaryTroops = false
	update_tick()
	$"/root/Game/%PreparingPanel".ShowTemporaryTroops = false
	$"/root/Game/%PreparingPanel".update_tick()
	$"/root/Game/%LaunchingPanel".ShowTemporaryTroops = false
	$"/root/Game/%LaunchingPanel".update_tick()

func update_shield_rings() -> void:
	# Get outpost data
	var HistoryEntry : Dictionary = get_history_entry(GameVariables.VisibleTick)
	# Get number of shield rings
	var MultipliedShieldMaximum : int = CreateItemOrder["ShieldMaximum"] * (1 + HistoryEntry["ShieldMaximumMultipliersTotal"])
	var ShieldRingNumber : int = ceil(MultipliedShieldMaximum / 10)
	# Sets shield rings visibility
	for child in $Body/Shield/ShieldRings.get_children():
		child.visible = child.get_index() == ShieldRingNumber - 1
	# Sets empty shield rings visibility
	for child in $Body/Shield/EmptyShieldRings.get_children():
		child.visible = child.get_index() == ShieldRingNumber - 1
	# Sets shield notches and shield label visibility
	if HistoryEntry["ShieldTotal"] > 0:
		$Body/Shield/ShieldLabel.show()
		$Body/Shield/ShieldNotches.show()
		for child in $Body/Shield/ShieldNotches.get_children():
			child.visible = child.get_index() == ShieldRingNumber - 1
	else:
		$Body/Shield/ShieldLabel.hide()
		$Body/Shield/ShieldNotches.hide()
	# Checks if a ring should be displayed
	if ShieldRingNumber > 0:
		# Set shield ring
		# Shield total / shield maximum * visible shield ring fraction (1.0 - 60.0 / 360.0) + shield ring start offset (30.0 / 360.0)
		var FillRatio : float = float(HistoryEntry["ShieldTotal"]) / float(MultipliedShieldMaximum) * 0.8333 + 0.0833
		$Body/Shield/ShieldRings.material.set_shader_parameter("fill_ratio", FillRatio)
		# Check that shield total should be displayed
		if HistoryEntry["ShieldTotal"] > 0:
			# Set shield notch
			var NotchRotation : float = 360.0 * FillRatio
			$Body/Shield/ShieldNotches.rotation_degrees = NotchRotation
			# Set shield label
			$Body/Shield/ShieldLabel.position = $Body/Shield.size / 2 + Vector2(105 + 15 * (ShieldRingNumber - 1), 0).rotated(deg_to_rad(NotchRotation)) - $Body/Shield/ShieldLabel.pivot_offset
			$Body/Shield/ShieldLabel.text = str(HistoryEntry["ShieldTotal"])

# Signals

func _on_Button_button_down() -> void:
	PreviousCursorPosition = get_global_mouse_position()

func _on_Button_button_up() -> void:
	if get_node("/root/Game").HoveredStartOutpost != self:
		select()

func _on_FocusArea_mouse_entered() -> void:
	focus()
	get_node("/root/Game").HoveredEndOutpost = self
	get_node("/root/Game").CursorPosition = CreateItemOrder["InitialPosition"]

func _on_FocusArea_mouse_exited() -> void:
	unfocus()
	if get_node("/root/Game").HoveredEndOutpost == self:
		get_node("/root/Game").HoveredEndOutpost = null
