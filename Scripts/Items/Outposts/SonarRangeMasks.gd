extends Node2D

var OutpostNode : Node

func _ready() -> void:
	for x in 3:
		for y in 3:
			var SonarRangeMask : Node = get_child(x * 3 + y)
			# Set mask position
			var WrapOffset : Vector2 = Vector2(x - 1, y - 1) * GameVariables.MapSize
			SonarRangeMask.position = WrapOffset

func update_masks() -> void:
	# Gets data
	var HistoryEntry : Dictionary = OutpostNode.get_history_entry(GameVariables.GlobalTick)
	# Get mask size
	# 1 / texture size * sonar range radius * 2 (make diameter) * sonar range mutiplier
	var SonarRangeTextureScale : float = 1.0 / 500.0 * GameVariables.DefaultSonarRange * GameVariables.SonarRangeMultiplier * 2 * (1 + HistoryEntry["SonarRangeMultipliersTotal"])
	# Get mask enabled
	var SonarRangeEnabled : bool = OutpostNode.get_sonar_status(GameVariables.VisibleTick)
	visible = SonarRangeEnabled
	# Updates sonar masks size
	if SonarRangeEnabled:
		for masknode in get_children():
			masknode.scale = Vector2(SonarRangeTextureScale, SonarRangeTextureScale)
