extends "res://Scripts/Items/Item.gd"

# Inbuilt functions

func _ready() -> void:
	super()
	# Create player leaderboard entry
	get_node("/root/Game/%LeaderboardPanel").create_leaderboard_entry(self)

# Data functions

# Calculate data
func calculate_data() -> void:
	# Get player default history entry
	var DefaultHistoryEntry : Dictionary = {
		"Tick" : CreateItemOrder["OrderTick"],
		# Totals variables
		"TroopsTotal" : 0,
		"OreTotal" : 0,
		"ChargeTotal" : 0,
		"OutpostsTotal" : 0
	}
	# Set player variables
	Data = {
		# Info
		"Type" : "Player",
		"History" : [DefaultHistoryEntry.duplicate(true)],
		"DefaultHistoryEntry" : DefaultHistoryEntry,
		# Player order variables
		"CreatePlayerOrderTick" : CreateItemOrder["OrderTick"],
		"CreatePlayerOrderId" : CreateItemOrder["OrderId"]
	}
	History = Data["History"]

# Active functions

# Set related items active
func set_related_items_active(tick : int) -> void:
	# Set player outposts active
	for outpostnode in get_node("/root/Game/Items/Outposts").get_children():
		# Get outpost data
		var OutpostHistoryEntry : Dictionary = outpostnode.get_history_entry(tick)
		var OutpostPlayer : String = OutpostHistoryEntry["CurrentPlayer"]
		# Check that outpost player is player
		if OutpostPlayer == name:
			# Set outpost active
			outpostnode.set_active(tick)
