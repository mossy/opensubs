extends "res://Scripts/Items/Item.gd"

# Initial outpost variables

var InitialOutpostNode : Node
var InitialOutpost : String

# History expansion variables

var Arrived : bool = false

# Data functions

# Calculate data
func calculate_data() -> void:
	# Set initial outpost variables
	InitialOutpostNode = get_node("/root/Game/Items/Outposts/" + CreateItemOrder["InitialOutpost"])
	InitialOutpost = InitialOutpostNode.name
	# Get initial position
	var InitialOutpostHistoryEntry : Dictionary = InitialOutpostNode.get_history_entry(CreateItemOrder["OrderTick"])
	var InitialPosition : Vector2 = InitialOutpostHistoryEntry["Position"]
	# Get submarine history
	var DefaultHistoryEntry : Dictionary = {
		"Tick" : CreateItemOrder["OrderTick"],
		# Status variables
		"Position" : InitialPosition,
		# Totals variables
		"TroopsTotal" : 0,
		"SpeedMultiplierTotal" : 0,
		# Current variables
		"CurrentPlayer" : "",
		"CurrentDetector" : "",
		"CurrentType" : "",
		"CurrentTarget" : "",
		# Effected lists
		"EffectedOutposts" : [],
		"EffectedSubmarines" : []
	}
	# Set submarine variables
	Data = {
		# Info
		"Name" : name,
		"Type" : "Submarine",
		"History" : [DefaultHistoryEntry.duplicate(true)],
		"DefaultHistoryEntry" : DefaultHistoryEntry,
		# Submarine order variables
		"CreateSubmarineOrderTick" : CreateItemOrder["OrderTick"],
		"CreateSubmarineOrderId" : CreateItemOrder["OrderId"]
	}
	History = Data["History"]

# History functions

# Set initial create order values
func set_initial_values(tick : int) -> void:
	# Check if tick is create order tick
	if tick == CreateItemOrder["OrderTick"]:
		# Set history values
		set_history_type(tick, "Submarine")
		set_history_target(tick, CreateItemOrder["InitialTarget"])
		set_history_player(tick, CreateItemOrder["InitialPlayer"])
		# TODO: remove false
		set_history_troops(tick, CreateItemOrder["InitialTroops"], false)

# Calculate history effect
func calculate_history_effect(tick : int) -> void:
	super(tick)
	# Check that order has not been canceled
	if CreateItemOrderNode.InvalidReasons.has("Canceled") == false:
		# Calculate arrival
		if Arrived:
			# Calculate effect on target outpost
			calculate_target_effect()
			# Set active false
			remove_active(tick)
			# Set arrived false
			Arrived = false
		# Calculate travel
		else:
			# Calculate effect on initial outpost
			calculate_initial_outpost_effect(tick)
			# Calculate combat
			calculate_combat(tick)

# Position functions

# Calculates position for passed tick
func calculate_position(tick : int) -> void:
	# Check that order has not been canceled
	if CreateItemOrderNode.InvalidReasons.has("Canceled") == false:
		# Get data
		var PreviousHistoryEntry : Dictionary = get_previous_history_entry(tick)
		var CurrentHistoryEntry : Dictionary = get_history_entry(tick)
		# Calculate position
		CurrentHistoryEntry["Position"] = PreviousHistoryEntry["Position"]
		if tick > History[0]["Tick"] + GameVariables.OrderWaitTicks:
			# Get target data
			var Target : String = PreviousHistoryEntry["CurrentTarget"]
			var TargetNode : Node = get_node("/root/Game").get_item_node(Target)
			var TargetHistoryEntry : Dictionary = TargetNode.get_history_entry(tick)
			# TODO: make work with submarines
			# Moves submarine speed from previous position towards previous target
			var WrappedTargetPosition : Vector2 = get_node("/root/Game").get_closest_wrapped_position(PreviousHistoryEntry["Position"], TargetHistoryEntry["Position"])
			var TargetAngle : float = PreviousHistoryEntry["Position"].angle_to_point(WrappedTargetPosition)
			var Speed : float = GameVariables.DefaultSubmarineSpeed * GameVariables.SubmarineSpeedMultiplier * (1 + PreviousHistoryEntry["SpeedMultiplierTotal"])
			var Displacement : Vector2 = Vector2(min(Speed, PreviousHistoryEntry["Position"].distance_to(WrappedTargetPosition)), 0).rotated(TargetAngle)
			CurrentHistoryEntry["Position"] = get_node("/root/Game").get_wrapped_position(PreviousHistoryEntry["Position"] + Displacement, Vector2())
			# Set submarine arrived
			if get_line_point_overlap([PreviousHistoryEntry["Position"], CurrentHistoryEntry["Position"]], TargetHistoryEntry["Position"]):
				Arrived = true
		# Update visible
		for outpostnode in get_node("/root/Game/Items/Outposts").get_children():
			outpostnode.calculate_obscured_item_visible(tick, self)

# Combat functions

# Checks if submarine should affect other items at passed tick
func get_effects(tick : int) -> bool:
	# Check that create order is valid
	if CreateItemOrderNode.InvalidReasons.is_empty():
		# Check that tick is before global tick
		if tick <= GameVariables.GlobalTick:
			return true
		# Check that submarine is visible at tick
		var HistoryEntry : Dictionary = get_history_entry(tick)
		if HistoryEntry["CurrentDetector"] != "":
			return true
	return false

# Get combat submarines for submarine at passed tick
func get_combat_submarines(tick : int) -> Array:
	# Get data
	var PreviousHistoryEntry : Dictionary = get_previous_history_entry(tick)
	var CurrentHistoryEntry : Dictionary = get_history_entry(tick)
	# Check submarines
	var CombatSubmarines : Array
	for combatsubmarinenode in get_node("/root/Game/Items/Submarines").get_children():
		# Check that combat submarine is not submarine
		var CombatSubmarine : String = combatsubmarinenode.name
		if CombatSubmarine != name:
			# Get combat submarine data
			var CombatSubmarineCreateSubmarineOrder : Dictionary = combatsubmarinenode.CreateItemOrder
			# Check that combat submarine is present before tick
			if tick > CombatSubmarineCreateSubmarineOrder["OrderTick"]:
				# Get combat submarine data
				var PreviousCombatSubmarineHistoryEntry : Dictionary = combatsubmarinenode.get_previous_history_entry(tick)
				var CurrentCombatSubmarineHistoryEntry : Dictionary = combatsubmarinenode.get_history_entry(tick)
				# Check that combat submarine history entries are not empty
				if PreviousCombatSubmarineHistoryEntry.is_empty() == false and CurrentCombatSubmarineHistoryEntry.is_empty() == false:
					# Check that submarines do not have same player
					if PreviousHistoryEntry["CurrentPlayer"] != PreviousCombatSubmarineHistoryEntry["CurrentPlayer"]:
						# Checks that submarines target other submarine initial outpost
						if (
							PreviousHistoryEntry["CurrentTarget"] == CombatSubmarineCreateSubmarineOrder["InitialOutpost"] and
							PreviousCombatSubmarineHistoryEntry["CurrentTarget"] == CreateItemOrder["InitialOutpost"]
						):
							# Get submarine positions
							var PreviousPosition : Vector2 = PreviousHistoryEntry["Position"]
							var CurrentPostion : Vector2 = CurrentHistoryEntry["Position"]
							# Get combat submarine positions
							var PreviousCombatSubmarinePosition : Vector2 = PreviousCombatSubmarineHistoryEntry["Position"]
							var CurrentCombatSubmarinePosition : Vector2 = CurrentCombatSubmarineHistoryEntry["Position"]
							var WrappedPreviousCombatSubmarinePosition : Vector2 = get_node("/root/Game").get_closest_wrapped_position(PreviousPosition, PreviousCombatSubmarinePosition)
							var WrappedCurrentCombatSubmarinePosition : Vector2 = get_node("/root/Game").get_closest_wrapped_position(CurrentPostion, CurrentCombatSubmarinePosition)
							# Checks that submarine positions overlap
							if get_lines_overlap(
								[PreviousPosition, CurrentPostion],
								[WrappedPreviousCombatSubmarinePosition, WrappedCurrentCombatSubmarinePosition]
							):
								# Check that combat submarine effects
								if combatsubmarinenode.get_effects(tick):
									CombatSubmarines.append(CombatSubmarine)
								break
	return CombatSubmarines

# Checks if a line overlaps a point
func get_line_point_overlap(line : Array, point : Vector2) -> bool:
	var LineRect : Rect2 = Rect2(line[0], Vector2()).expand(line[1])
	var PointRect : Rect2 = Rect2(point, Vector2())
	return LineRect.intersects(PointRect, true)

# Checks if two lines overlap
func get_lines_overlap(firstline : Array, secondline : Array) -> bool:
	var FirstLineRect : Rect2 = Rect2(firstline[0], Vector2()).expand(firstline[1])
	var SecondLineRect : Rect2 = Rect2(secondline[0], Vector2()).expand(secondline[1])
	return FirstLineRect.intersects(SecondLineRect, true)

# Calculate combat
func calculate_combat(tick : int) -> void:
	for combatsubmarine in get_combat_submarines(tick):
		calculate_combat_submarine_attack(tick, combatsubmarine)

# Calculate effect on initial outpost
func calculate_initial_outpost_effect(tick : int) -> void:
	# Check that tick if create submarine order tick
	if tick == CreateItemOrder["OrderTick"]:
		# Get data
		var HistoryEntry : Dictionary = History[0]
		var InitialOutpostHistoryEntry : Dictionary = InitialOutpostNode.get_history_entry(CreateItemOrder["OrderTick"])
		# Set initial outpost in effected outposts
		HistoryEntry["EffectedOutposts"].append(InitialOutpost)
		# Set submarine in initial outpost effected submarines
		InitialOutpostHistoryEntry["EffectedSubmarines"].append(name)
		# Check that submarine effects
		if get_effects(tick):
			print("Set Outposts " + InitialOutpost + " launched " + name + " at " + str(tick))
			# TODO: remove
			# Get initial player
			var InitialPlayerNode : Node = get_node("/root/Game/Items/Players/" + CreateItemOrder["InitialPlayer"])
			# Set initial outpost active
			InitialOutpostNode.set_active(tick)
			# Set troops
			InitialOutpostNode.set_history_troops(tick, -CreateItemOrder["InitialTroops"])
			InitialPlayerNode.set_history_troops(tick, CreateItemOrder["InitialTroops"])

# Calculate effect on target
func calculate_target_effect() -> void:
	# Get data
	var HistoryEntry : Dictionary = History[-1]
	# Get target data
	var Target : String = HistoryEntry["CurrentTarget"]
	var TargetNode : Node = get_node("/root/Game").get_item_node(Target)
	var TargetData : Dictionary = TargetNode.Data
	# Calculate target effect
	match TargetData["Type"]:
		"Outpost":
			calculate_combat_outpost_attack(HistoryEntry["Tick"], Target)
		"Submarine":
			calculate_combat_submarine_attack(HistoryEntry["Tick"], Target)

# Calculate combat outpost attack
func calculate_combat_outpost_attack(tick : int, combatoutpost : String) -> void:
	print("Set Outposts " + combatoutpost + " attack by " + name + " at " + str(tick))
	# Set combat outpost active
	var CombatOutpostNode : Node = get_node("/root/Game/Items/Outposts/" + combatoutpost)
	CombatOutpostNode.set_active(tick)
	# Get data
	var HistoryEntry : Dictionary = get_history_entry(tick)
	var CombatOutpostHistoryEntry : Dictionary = CombatOutpostNode.get_history_entry(tick)
	# Set items in effected lists
	HistoryEntry["EffectedOutposts"].append(combatoutpost)
	CombatOutpostHistoryEntry["EffectedSubmarines"].append(name)
	# Check that submarine effects
	if get_effects(tick):
		# Calculate nonaggressive attack
		if (
			HistoryEntry["CurrentType"] == "Gift" or
			CombatOutpostHistoryEntry["CurrentPlayer"] == "Dormant" or
			CombatOutpostHistoryEntry["CurrentPlayer"] == HistoryEntry["CurrentPlayer"]
		):
			# Set combat outpost captured
			if CombatOutpostHistoryEntry["CurrentPlayer"] == "Dormant":
				CombatOutpostNode.set_history_player(tick, HistoryEntry["CurrentPlayer"])
				CombatOutpostNode.update_sonar(tick)
			# Get attack values
			var RemainingTroops : int = HistoryEntry["TroopsTotal"]
			# Set combat outpost attack values
			CombatOutpostNode.set_history_troops(tick, RemainingTroops)
		# Calculate aggressive attack
		else:
			# Get attack values
			var LostShieldMaximum : int = CombatOutpostHistoryEntry["ShieldTotal"]
			var LostTroopsMaximum : int = CombatOutpostHistoryEntry["TroopsTotal"]
			var LostShield : int = min(HistoryEntry["TroopsTotal"], LostShieldMaximum)
			var LostTroops : int = min(HistoryEntry["TroopsTotal"] - LostShield, LostTroopsMaximum)
			var RemainingTroops : int = HistoryEntry["TroopsTotal"] - LostShield - LostTroops
			# Set combat outpost attack values
			CombatOutpostNode.set_history_shield(tick, -LostShield)
			CombatOutpostNode.set_history_troops(tick, -LostTroops)
			# Set outpost captured
			if RemainingTroops > 0:
				CombatOutpostNode.set_history_player(tick, HistoryEntry["CurrentPlayer"])
				CombatOutpostNode.set_history_troops(tick, RemainingTroops)
				# TODO: consider move to outpost update_player_related_variables 
				CombatOutpostNode.update_sonar(tick)
	# Set troops lost
	set_history_troops(tick, -HistoryEntry["TroopsTotal"])
	# Remove detector
	set_history_detector(tick, "")

# TODO: rework
# Calculate combat submarine attack
func calculate_combat_submarine_attack(tick : int, combatsubmarine : String) -> void:
	print("Set Submarines " + combatsubmarine + " attack by " + name + " at " + str(tick))
	# Get data
	var PreviousHistoryEntry : Dictionary = get_previous_history_entry(tick)
	var CurrentHistoryEntry : Dictionary = get_history_entry(tick)
	# Get combat submarine data
	var CombatSubmarineNode : Node = get_node("/root/Game/Items/Submarines/" + combatsubmarine)
	var PreviousCombatSubmarineHistoryEntry : Dictionary = CombatSubmarineNode.get_previous_history_entry(tick)
	var CurrentCombatSubmarineHistoryEntry : Dictionary = CombatSubmarineNode.get_history_entry(tick)
	# Set combat submarine active
	CombatSubmarineNode.set_active(tick)
	# Set combat submarine in effected submarines
	CurrentHistoryEntry["EffectedSubmarines"].append(combatsubmarine)
	# Check that submarine effects
	if get_effects(tick):
		# Get player data
		var Player : String = CurrentHistoryEntry["CurrentPlayer"]
		# Calculate nonaggressive attack
		if PreviousHistoryEntry["CurrentType"] != "Gift" and PreviousCombatSubmarineHistoryEntry["CurrentType"] == "Gift":
			# Set combat submarine captured
			CombatSubmarineNode.set_history_player(tick, Player)
			CombatSubmarineNode.set_history_type(tick, "Submarine")
		# Calculate aggressive attack
		elif PreviousHistoryEntry["CurrentType"] != "Gift" and PreviousCombatSubmarineHistoryEntry["CurrentType"] != "Gift":
			# Get attack values
			var LostTroopsMaximum : int = PreviousHistoryEntry["TroopsTotal"]
			var LostTroops : int = min(CurrentCombatSubmarineHistoryEntry["TroopsTotal"], LostTroopsMaximum)
			# Set combat submarine attack values
			CombatSubmarineNode.set_history_troops(tick, -LostTroops)
			# Set combat submarine captured
			if CurrentCombatSubmarineHistoryEntry["TroopsTotal"] <= 0:
				CombatSubmarineNode.set_history_player(tick, Player)
