extends "res://Scripts/Items/Submarines/Submarine.gd"

@export var OutlineWidth : int = 15

var SubmarineLinesScene : PackedScene = preload("res://Scenes/Lines/SubmarineLines.tscn")

var SubmarineLinesInstance : Node

var Selected : bool = false
var Focused : bool = false
var ShowTemporaryTroops : bool = false

# Inbuilt funciton

func _ready() -> void:
	super()
	$Body/Textures.set_material(Shaders.Submarine.duplicate())
	$Body/Textures.material.set_shader_parameter("width", OutlineWidth)
	# Get submarine lines instance
	SubmarineLinesInstance = SubmarineLinesScene.instantiate()
	SubmarineLinesInstance.SubmarineNode = self
	get_node("/root/Game/Lines").add_child(SubmarineLinesInstance)

# Tick functions

# Updates the submarine to show it's state at the passed tick
func update_tick() -> void:
	# Get submarine data
	var HistoryEntry : Dictionary = get_history_entry(GameVariables.VisibleTick)
	# Check that history entry is not empty
	if HistoryEntry.is_empty() == false and HistoryEntry["Tick"] < History[-1]["Tick"]:
		show()
		$Body/Textures.material.set_shader_parameter("bodycolor", Color(GameVariables.Colors.get(HistoryEntry["CurrentPlayer"])))
		# Updates position
		update_position()
		# Set submarine panels
		set_panels()
		# Hide submarine
		if HistoryEntry["CurrentDetector"] == "":
			hide_submarine()
		# Check if order is valid
		elif CreateItemOrderNode.InvalidReasons.is_empty():
			# Set troops label
			if ShowTemporaryTroops == false:
				$Body/TroopsLabel.text = str(HistoryEntry["TroopsTotal"])
			# Check if submarine is launching
			if CreateItemOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick):
				# Set line
				SubmarineLinesInstance.switch_line("Preparing")
				# Play prepare move animation
				if $PrepareMoveAnimation.is_playing() == false:
					$PrepareMoveAnimation.play("PrepareMove")
			else:
				# Set line
				SubmarineLinesInstance.switch_line("Launched")
				# stop prepare move animation
				$PrepareMoveAnimation.stop()
		else:
			# Set troops label
			if ShowTemporaryTroops == false:
				$Body/TroopsLabel.text = str(CreateItemOrder["InitialTroops"])
			# Check if submarine is launching
			if CreateItemOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick):
				# Set line
				SubmarineLinesInstance.switch_line("Invalid")
				# Play prepare move animation
				if $PrepareMoveAnimation.is_playing() == false:
					$PrepareMoveAnimation.play("PrepareMove")
				# Check if initial outpost has been captured
				if CreateItemOrderNode.InvalidReasons.has("OutpostCaptured"):
					# Hide submarine
					hide_submarine()
			else:
				# Hide submarine
				hide_submarine()
		# Set gift flash animation
		var GiftOrderNode : Node = get_previous_item_order_nodes(GameVariables.VisibleTick, "GiftSubmarine", true)[-1]
		if GiftOrderNode.is_inside_tree():
			# Check that gift order is within wait period
			if GiftOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick):
				$GiftFlashAnimation.play("GiftFlash")
			else:
				$GiftFlashAnimation.stop()
		else:
			$GiftFlashAnimation.stop()
		# Set submarine body
		if $GiftFlashAnimation.is_playing() == false:
			$Body/Textures/Gift.visible = HistoryEntry["CurrentType"] == "Gift"
			$Body/Textures/Submarine.visible = HistoryEntry["CurrentType"] == "Submarine"
	else:
		# Focus target
		if Selected:
			if CreateItemOrderNode.InvalidReasons.is_empty():
				# TODO: make work with submarines
				# Focus target outpost
				get_node("/root/Game/Items/Outposts/" + History[-1]["CurrentTarget"]).select()
		# Hide submarine
		hide_submarine()

# Data functions

# Points submarine in direction of target
func set_course(targetposition : Vector2) -> void:
	var DirectionAngle : float = global_position.angle_to_point(targetposition)
	rotation = DirectionAngle
	var Flipped : bool = rad_to_deg(DirectionAngle) > 90 or rad_to_deg(DirectionAngle) < -90
	if Flipped:
		scale.y = -1
		$Body/TroopsLabel.scale.x = -1
	else:
		scale.y = 1
		$Body/TroopsLabel.scale.x = 1

# Visual functions

# Sets the submarine's current position and rotation
func update_position() -> void:
	# Get submarine data
	var HistoryEntry : Dictionary = get_history_entry(GameVariables.VisibleTick)
	# Check that history entry is not empty
	if HistoryEntry.is_empty() == false:
		# Get target data
		var Target : String = HistoryEntry["CurrentTarget"]
		var TargetNode : Node = get_node("/root/Game").get_item_node(Target)
		var TargetHistoryEntry : Dictionary = TargetNode.get_history_entry(GameVariables.VisibleTick)
		# Set position
		var WrappedPosition : Vector2 = get_node("/root/Game").get_wrapped_position(HistoryEntry["Position"])
		global_position = WrappedPosition
		# Set rotation
		var TargetPosition : Vector2 = TargetHistoryEntry["Position"]
		var WrappedTargetPosition : Vector2 = get_node("/root/Game").get_closest_wrapped_position(HistoryEntry["Position"], TargetPosition)
		var RelativeTargetPosition : Vector2 = global_position - (HistoryEntry["Position"] - WrappedTargetPosition)
		SubmarineLinesInstance.set_line_points([global_position, RelativeTargetPosition])
		set_course(RelativeTargetPosition)

# Set submarine panels
func set_panels() -> void:
	if Selected:
		# Get submarine data
		var HistoryEntry : Dictionary = get_history_entry(GameVariables.VisibleTick)
		# Check if submarine is visible
		if HistoryEntry["CurrentDetector"] != "":
			# Check if submarine is launching
			if (
				HistoryEntry["CurrentPlayer"] == GameVariables.Player and
				CreateItemOrderNode.get_awaiting_order_effect(GameVariables.VisibleTick) and
				GameVariables.VisibleTick >= GameVariables.GlobalTick
			):
				# Set panels for launching submarine
				get_node("/root/Game").show_submarine_panels(["PreparingPanel", "LaunchingPanel"], self)
			
			else:
				# Set panels for travelling submarine
				get_node("/root/Game").show_submarine_panels(["SubmarinePanel"], self)
		else:
			# Set panels for obscured submarine
			get_node("/root/Game").show_submarine_panels(["ObscuredSubmarinePanel"], self)

# Focus submarine
func focus() -> void:
	if Focused == false:
		$FocusAnimation.play("Focus")
	Focused = true

func unfocus() -> void:
	if Focused:
		$FocusAnimation.play("Unfocus")
	Focused = false

func select(movecamera : bool = false) -> void:
	if History[0]["Tick"] <= GameVariables.VisibleTick and History[-1]["Tick"] >= GameVariables.VisibleTick:
		# Unselect items
		get_node("/root/Game").unselect_outposts()
		get_node("/root/Game").unselect_submarines(name)
		# Set camera position to submarine position
		if movecamera:
			get_node("/root/Game").set_camera_position(global_position)
		# Set panels submarine to submarine
		for panelnode in get_node("/root/Game/Interface/Panels/Outposts").get_children():
			panelnode.Outpost = InitialOutpost
		# Set panels outpost to submarine initial outpost
		for panelnode in get_node("/root/Game/Interface/Panels/Submarines").get_children():
			panelnode.Submarine = name
		# Set selected true
		Selected = true
		$Body/Textures.material.set_shader_parameter("selected", true)
		# Switch panels
		set_panels()

func deselect() -> void:
	# Set selected false
	Selected = false
	$Body/Textures.material.set_shader_parameter("selected", false)

# Hides the submarine
func hide_submarine() -> void:
	# Hide elements
	unfocus()
	SubmarineLinesInstance.hide_lines()
	hide()
	# Hide submarine panels
	if Selected:
		get_node("/root/Game").show_submarine_panels([], self)

# Temporarily changes troops
func set_temporary_troops(troops) -> void:
	ShowTemporaryTroops = true
	$Body/TroopsLabel.text = str(troops)

func override_temporary_troops() -> void:
	ShowTemporaryTroops = false
	update_tick()

# Signals

func _on_Submarine_tree_exiting():
	if get_node("/root/Game").Exiting == false:
		hide_submarine()
		SubmarineLinesInstance.LineButtonsInstance.queue_free()
		SubmarineLinesInstance.queue_free()

# Makes submarine panels visible
func _on_Button_pressed() -> void:
	select()
