extends Node2D

signal pressed

func _ready() -> void:
	for x in 3:
		for y in 3:
			var LineButton : Node = get_child(x * 3 + y)
			# Set line position
			var WrapOffset : Vector2 = Vector2(x - 1, y - 1) * GameVariables.MapSize
			LineButton.position = WrapOffset

func set_line_points(linepoints : Array) -> void:
	var LineDistance : float = linepoints[0].distance_to(linepoints[1])
	var LinePosition : Vector2 = linepoints[0] - get_child(0).get_node("Button").pivot_offset
	var LineRotation : float = rad_to_deg(linepoints[1].angle_to_point(linepoints[0]))
	for child in get_children():
		child.get_node("Button").size.x = LineDistance
		child.get_node("Button").position = LinePosition
		child.get_node("Button").rotation_degrees = LineRotation

func _on_Button_pressed() -> void:
	emit_signal("pressed")
