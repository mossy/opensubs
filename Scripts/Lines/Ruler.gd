extends Node2D

var RulerStartTick : int

# TODO: move hovered start outpost

func _process(delta : float) -> void:
	if Input.is_action_just_released("Click"):
		if get_node("/root/Game").HoveredStartOutpost != null:
			if get_node("/root/Game").HoveredEndOutpost != null:
				set_create_submarine_order()
			get_node("/root/Game").set_visible_tick(RulerStartTick)
			get_node("/root/Game").HoveredStartOutpost = null
			get_node("/root/Game").HoveredEndOutpost = null
			hide()
	elif get_node("/root/Game").HoveredStartOutpost != null:
		show()
		# Show time panel
		get_node("/root/Game/%TimePanel").show()
		# Update rulers
		var HoveredStartOutpostPosition : Vector2 = get_node("/root/Game").HoveredStartOutpost.global_position
		var CursorWrappedPosition : Vector2 = get_node("/root/Game").get_closest_wrapped_position(HoveredStartOutpostPosition, get_node("/root/Game").CursorPosition)
		var TicksToArrival : int = floor(HoveredStartOutpostPosition.distance_to(CursorWrappedPosition) / (GameVariables.DefaultSubmarineSpeed * GameVariables.SubmarineSpeedMultiplier) + GameVariables.OrderWaitTicks)
		for x in 3:
			for y in 3:
				var RulerLine : Node = get_child(x * 3 + y)
				# Set line points
				var WrapOffset : Vector2 = Vector2(x - 1, y - 1) * GameVariables.MapSize
				RulerLine.get_node("Line2D").points = [HoveredStartOutpostPosition + WrapOffset, CursorWrappedPosition + WrapOffset]
				# Set label text
				var RulerLabelPosition : Vector2 = get_node("/root/Game").CursorPosition - RulerLine.get_node("Label").pivot_offset
				if get_node("/root/Game").HoveredEndOutpost == null:
					RulerLabelPosition -= Vector2(0, 20)
				else:
					RulerLabelPosition -= Vector2(0, 80)
				RulerLine.get_node("Label").position = RulerLabelPosition
				RulerLine.get_node("Label").text = str(VariableFunctions.get_time_text(TicksToArrival * GameVariables.TickLength))
		# Set visible tick
		get_node("/root/Game").set_visible_tick(RulerStartTick + TicksToArrival)
		get_node("/root/Game/%TimeButton").button_pressed = true

# TODO: make work with submarines
# Set create submarine order
func set_create_submarine_order() -> void:
	# Get hovered start outpost data
	var HoveredStartOutpostHistory : Array = get_node("/root/Game").HoveredStartOutpost.History
	var HoveredStartOutpostHistoryIndex : int = get_node("/root/Game").HoveredStartOutpost.get_history_index(RulerStartTick)
	var HoveredStartOutpostHistoryEntry : Dictionary = HoveredStartOutpostHistory[HoveredStartOutpostHistoryIndex]
	# Check that hovered start outpost belongs to player
	if HoveredStartOutpostHistoryEntry["CurrentPlayer"] == GameVariables.Player:
		# Focus existing submarine
		for i in GameVariables.OrderWaitTicks:
			# Get hovered start outpost data
			var CurrentHoveredStartOutpostHistoryIndex = HoveredStartOutpostHistoryIndex - i
			# Check that index exists in start outpost history
			if CurrentHoveredStartOutpostHistoryIndex >= 0:
				# Get hovered start outpost data
				var CurrentHoveredStartOutpostHistoryEntry = HoveredStartOutpostHistory[CurrentHoveredStartOutpostHistoryIndex]
				# Check effected submarines
				for outpostsubmarine in CurrentHoveredStartOutpostHistoryEntry["EffectedSubmarines"]:
					# Get outpost submarine data
					var OutpostSubmarineNode : Node = get_node("/root/Game/Items/Submarines/" + outpostsubmarine)
					var OutpostSubmarineHistoryEntry : Dictionary = OutpostSubmarineNode.get_history_entry(RulerStartTick)
					# Check that outpost submarine is targeting hovered end outpost
					if OutpostSubmarineHistoryEntry.is_empty() == false and OutpostSubmarineHistoryEntry["CurrentTarget"] == get_node("/root/Game").HoveredEndOutpost.name:
						# Select outpost submarine
						get_node("/root/Game").set_visible_tick(RulerStartTick)
						OutpostSubmarineNode.select()
						return
		# Get create submarine order
		var SubmarineOrdersNode : Node = get_node("/root/Game/Orders/Submarines/")
		var CreateSubmarineOrder : Dictionary = {
			# Order variables
			"Time" : SubmarineOrdersNode.get_order_time(),
			"Type" : "CreateSubmarine",
			"OrderTick" : RulerStartTick,
			"OrderId" : randi(),
			"User" : Settings.User,
			# Create submarine order variables
			"Submarine" : "",
			"InitialPlayer" : GameVariables.Player,
			"InitialOutpost" : get_node("/root/Game").HoveredStartOutpost.name,
			"InitialTarget" : get_node("/root/Game").HoveredEndOutpost.name,
			"InitialTroops" : 0
		}
		# Set create submarine order
		Network.rpc_id(
			1,
			"set_server_order",
			# Game ID
			GameVariables.GameId,
			# Client ID
			multiplayer.get_unique_id(),
			# Order
			CreateSubmarineOrder
		)
		SubmarineOrdersNode.create_order(CreateSubmarineOrder)
		# Select submarine
		var SubmarineNode : Node = get_node("/root/Game/Items/Submarines/" + CreateSubmarineOrder["Submarine"])
		SubmarineNode.select()
