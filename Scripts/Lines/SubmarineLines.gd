extends Node2D

var LineButtonsScene : PackedScene = preload("res://Scenes/Lines/LineButtons.tscn")

var SubmarineNode : Node

var LineButtonsInstance : Node

func _ready() -> void:
	# Set line buttons instance
	LineButtonsInstance = LineButtonsScene.instantiate()
	get_node("/root/Game/LineButtons").add_child(LineButtonsInstance)
	# Connect pressed signal
	LineButtonsInstance.connect("pressed", line_button_pressed)
	# TODO: move
	# Set lines wrap offset
	for x in 3:
		for y in 3:
			var Lines : Node = get_child(x * 3 + y)
			# Set line position
			var WrapOffset : Vector2 = Vector2(x - 1, y - 1) * GameVariables.MapSize
			Lines.position = WrapOffset

func line_button_pressed() -> void:
	SubmarineNode.select()

func set_line_points(linepoints : Array) -> void:
	for linesnodes in get_children():
		for linenodes in linesnodes.get_children():
			linenodes.points = linepoints
	LineButtonsInstance.set_line_points(linepoints)

func switch_line(linestring : String) -> void:
	show_lines()
	for linesnodes in get_children():
		for linenodes in linesnodes.get_children():
			linenodes.visible = linenodes.name == linestring

func show_lines() -> void:
	LineButtonsInstance.show()
	show()

func hide_lines() -> void:
	LineButtonsInstance.hide()
	hide()
