extends Control

var Config := ConfigFile.new()

# Inbuilt functions

func _ready() -> void:
	var ConfigLoad := Config.load(get_config_path())
	if ConfigLoad != OK:
		$"%SelectServerPopup".show()
	else:
		# Sets settings variables
		Settings.User = Config.get_value("Account", "User")
		Settings.ServerIp = Config.get_value("Connection", "ServerIp")
		Settings.ServerPort = Config.get_value("Connection", "ServerPort")
		# Checks if connection settings are valid
		if get_connection_settings_valid():
			# Gets games
			if multiplayer.get_peers().is_empty():
				Network.connect_to_server()
			# Shows main menu panel
			$"%MainMenuPanel".show()

# Config functions

# Returns the location of the config file
func get_config_path() -> String:
	return OS.get_executable_path().get_base_dir() + "/Client.cfg"

# Returns true if connection settings are valid
func get_connection_settings_valid() -> bool:
	if Settings.User == "":
		$"%InputUserPopup".show()
		return false
	elif Settings.ServerIp == "":
		$"%InputServerIpPopup".show()
		return false
	elif Settings.ServerPort == 0:
		$"%InputServerPortPopup".show()
		return false
	return true

# Menu functions

func show_game_info(gameid : int) -> void:
	for child in $Panels.get_children():
		if child.visible:
			$"%GameInfoMenuPanel".BackMenu = child.name
	$"%GameInfoMenuPanel".GameId = gameid
	$"%GameInfoMenuPanel".show()

func connection_succeeded() -> void:
	$"%SettingsMenuPanel".stop_connection_timer()
	$"%ConnectingPopup".hide()
	$"%ConnectionSucceededPopup".show()

func connection_failed() -> void:
	$"%ConnectingPopup".hide()
	$"%ConnectionFailedPopup".show()

# Prompt buttons

func _on_InputUserOkButton_pressed() -> void:
	$"%InputUserPopup".hide()
	$"%SettingsMenuPanel".show()

func _on_InputServerIpOkButton_pressed() -> void:
	$"%InputServerIpPopup".hide()
	$"%SettingsMenuPanel".show()

func _on_InputSeverPortOkButton_pressed() -> void:
	$"%InputServerPortPopup".hide()
	$"%SettingsMenuPanel".show()
