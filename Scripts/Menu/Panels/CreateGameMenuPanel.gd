extends "res://Scripts/Menu/Panels/MenuPanel.gd"

# Inbuilt functions

func _ready() -> void:
	for ticklength in GameVariables.TickLengthsList:
		$"%TickLength/OptionButton".add_item(VariableFunctions.get_time_text(ticklength, true))
	for victorytype in GameVariables.VictoryTypesList:
		$"%VictoryType/OptionButton".add_item(victorytype.capitalize())
	for maptype in GameVariables.MapTypesList:
		$"%MapType/OptionButton".add_item(maptype.capitalize())
	for outpostname in GameVariables.OutpostNamesList:
		$"%OutpostNames/OptionButton".add_item(outpostname.capitalize())

func _on_CreateGamePanel_visibility_changed() -> void:
	if visible:
		# Set starting inputs
		$"%GameName/LineEdit".text = ""
		_on_GameNameLineEdit_text_changed($"%GameName/LineEdit".text)
		$"%TickLength/OptionButton".select(6)
		_on_TickLengthOptionButton_item_selected($"%TickLength/OptionButton".get_selected_id())

# Input update functions

func _on_GameNameLineEdit_text_changed(newtext : String) -> void:
	$"%GameName/ErrorToolTipButton".visible = newtext == ""

func _on_TickLengthOptionButton_item_selected(index : int) -> void:
	$"%TickLength/WarningToolTipButton".visible = GameVariables.TickLengthsList[index] < 10

func _on_VictoryTypeOptionButton_item_selected(index):
	_on_VictoryOptionsButton_toggled($ScrollContainer/SetupInterfaceElements/VictoryOptionsButton.button_pressed)

# Sub menu toggle functions

func _on_VictoryOptionsButton_toggled(buttonpressed : bool) -> void:
	for child in $ScrollContainer/SetupInterfaceElements/VictoryOptions/Panel/VBoxContainer.get_children():
		child.visible = child.is_in_group(GameVariables.VictoryTypesList[$"%VictoryType/OptionButton".selected])
	$ScrollContainer/SetupInterfaceElements/VictoryOptions.visible = buttonpressed

func _on_MapOptionsButton_toggled(buttonpressed : bool) -> void:
	$ScrollContainer/SetupInterfaceElements/MapOptions.visible = buttonpressed

func _on_MultiplierOptionsButton_toggled(buttonpressed : bool) -> void:
	$ScrollContainer/SetupInterfaceElements/MultiplierOptions.visible = buttonpressed

func _on_AdvancedOptionsButton_toggled(buttonpressed : bool) -> void:
	$ScrollContainer/SetupInterfaceElements/AdvancedOptions.visible = buttonpressed

# Create game functions

func _on_CreateGameButton_pressed() -> void:
	# Get game info
	var GameName : String = $"%GameName/LineEdit".text
	if GameName == "":
		# Show input game name popup
		get_node("/root/Menu/%InputGameNamePopup").show()
		return
	var GameInfo : Dictionary = {
		# Basic setup variables
		"GameName" : GameName,
		"PlayerCount" : $"%PlayerCount/ScrollWheel".StepValue,
		"TickLength" : GameVariables.TickLengthsList[$"%TickLength/OptionButton".selected],
		# Victory setup variables
		"VictoryType" : GameVariables.VictoryTypesList[$"%VictoryType/OptionButton".selected],
		"DominationVictoryPercentage" : $"%DominationVictoryPercentage/ScrollWheel".StepValue,
		"MiningVictoryTotal" : $"%MiningVictoryTotal/ScrollWheel".StepValue,
		# Map setup variables
		"MapType" : GameVariables.MapTypesList[$"%MapType/OptionButton".selected],
		"OutpostsPerPlayer" : $"%OutpostsPerPlayer/ScrollWheel".StepValue,
		"OutpostNames" : GameVariables.OutpostNamesList[$"%OutpostNames/OptionButton".selected],
		# Multiplier setup variables
		"GenerateShieldMaximumTicksDivisor" : $"%GenerateShieldMaximumTicksDivisor/ScrollWheel".StepValue,
		"GenerateTroopsTicksDivisor" : $"%GenerateTroopsTicksDivisor/ScrollWheel".StepValue,
		"GenerateOreTicksDivisor" : $"%GenerateOreTicksDivisor/ScrollWheel".StepValue,
		"SonarRangeMultiplier" : $"%SonarRangeMultiplier/ScrollWheel".StepValue,
		"SubmarineSpeedMultiplier" : $"%SubmarineSpeedMultiplier/ScrollWheel".StepValue,
		# Advanced setup variables
		"StartWaitTicks" : $"%StartWaitTicks/ScrollWheel".StepValue,
		"OrderWaitTicks" : $"%OrderWaitTicks/ScrollWheel".StepValue,
		# Unimplemented setup variables
		"InitialOutpostTroops" : 40,
		"InitialOutpostShield" : 0,
		"OutpostSpacing" : 300,
		"GenerateTroopsNumber" : 6,
		"MineTroopsCost" : 50
	}
	Network.rpc_id(1, "create_game", multiplayer.get_unique_id(), Settings.User, GameInfo)
	# Show joined games menu panel
	get_node("/root/Menu/%JoinedGamesMenuPanel").show()
