extends "res://Scripts/Menu/Panels/MenuPanel.gd"

var GameInfoUserListItemScene : PackedScene = preload("res://Scenes/InterfaceElements/GameInfoUserListItem.tscn")

var GameId : int

# Set game info
func set_game_info(gameinfo : Dictionary) -> void:
	for variable in gameinfo:
		var SetupVariableNode : Node = $ScrollContainer/GameInfoElements/Setup.get_node_or_null(variable)
		if SetupVariableNode != null:
			SetupVariableNode.set_value(gameinfo[variable])
	# Set occupied players
	for user in gameinfo["Users"]:
		# Set game info player button instance
		var GameInfoUserListItemInstance : Node = GameInfoUserListItemScene.instantiate()
		GameInfoUserListItemInstance.User = user
		$ScrollContainer/GameInfoElements/Players.add_child(GameInfoUserListItemInstance)
	# Set unoccupied players
	for i in gameinfo["PlayerCount"] - len(gameinfo["Users"]):
		# Set game info player button instance
		var GameInfoUserListItemInstance : Node = GameInfoUserListItemScene.instantiate()
		$ScrollContainer/GameInfoElements/Players.add_child(GameInfoUserListItemInstance)

func _on_GameInfoMenuPanel_visibility_changed() -> void:
	if visible:
		# Remove players
		for child in $ScrollContainer/GameInfoElements/Players.get_children():
			if child.is_in_group("Permanent") == false:
				child.queue_free()
		# Get game info
		Network.rpc_id(1, "get_game_info", GameId, multiplayer.get_unique_id())

func _on_AdvancedOptionsButton_toggled(buttonpressed : bool) -> void:
	# Toggle advanded options
	for child in $ScrollContainer/GameInfoElements/Setup.get_children():
		child.visible = child.is_in_group("Advanced") == false or buttonpressed
