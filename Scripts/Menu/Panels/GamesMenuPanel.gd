extends "res://Scripts/Menu/Panels/MenuPanel.gd"

func _on_JoinedGamesMenuButton_pressed() -> void:
	# Show joined games menu panel
	get_node("/root/Menu/%JoinedGamesMenuPanel").show()

func _on_PublicGamesMenuButton_pressed() -> void:
	# Show public games menu panel
	get_node("/root/Menu/%PublicGamesMenuPanel").show()

func _on_CreateGameMenuButton_pressed() -> void:
	# Show create game menu panel
	get_node("/root/Menu/%CreateGameMenuPanel").show()
