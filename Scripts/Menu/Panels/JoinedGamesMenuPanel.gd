extends "res://Scripts/Menu/Panels/MenuPanel.gd"

var JoinedGameButtonScene : PackedScene = preload("res://Scenes/GameButtons/JoinedGameButton.tscn")

# Set joined games
func set_games(gamesinfo : Dictionary) -> void:
	for child in $ScrollContainer/GameButtons.get_children():
		if child.is_in_group("Permanent") == false:
			child.queue_free()
	for gameid in gamesinfo.keys():
		# Set joined game button instance
		var JoinedGameButtonInstance : Node = JoinedGameButtonScene.instantiate()
		JoinedGameButtonInstance.set_game(gameid, gamesinfo[gameid])
		$ScrollContainer/GameButtons.add_child(JoinedGameButtonInstance)
		$ScrollContainer/GameButtons.move_child(JoinedGameButtonInstance, 1)

func _on_JoinedGamesMenuPanel_visibility_changed() -> void:
	if visible:
		# Get joined games
		Network.rpc_id(1, "get_joined_games", multiplayer.get_unique_id(), Settings.User)

func _on_RefreshGamesButton_pressed() -> void:
	# Refresh joined games
	Network.rpc_id(1, "get_joined_games", multiplayer.get_unique_id(), Settings.User)
