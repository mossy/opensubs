extends "res://Scripts/Menu/Panels/MenuPanel.gd"

func _on_GamesMenuButton_pressed() -> void:
	# Show games menu panel
	get_node("/root/Menu/%GamesMenuPanel").show()

func _on_SettingsMenuButton_pressed() -> void:
	# Show settings menu panel
	get_node("/root/Menu/%SettingsMenuPanel").show()
