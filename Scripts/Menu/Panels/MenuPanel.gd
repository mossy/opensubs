extends Panel

@export var BackMenu : String

func _on_BackButton_pressed() -> void:
	# Show back menu panel
	get_node("/root/Menu/%" + BackMenu).show()

func _on_MenuPanel_visibility_changed():
	if visible:
		# Hide other menu panels
		for child in get_parent().get_children():
			if child != self:
				child.hide()
