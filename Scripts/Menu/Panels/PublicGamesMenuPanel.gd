extends "res://Scripts/Menu/Panels/MenuPanel.gd"

var PublicGameButtonScene : PackedScene = preload("res://Scenes/GameButtons/PublicGameButton.tscn")

# Set public games
func set_games(gamesinfo : Dictionary) -> void:
	for child in $ScrollContainer/GameButtons.get_children():
		if child.is_in_group("Permanent") == false:
			child.queue_free()
	for gameid in gamesinfo.keys():
		# Set public game button instance
		var PublicGameButtonInstance : Node = PublicGameButtonScene.instantiate()
		PublicGameButtonInstance.set_game(gameid, gamesinfo[gameid])
		$ScrollContainer/GameButtons.add_child(PublicGameButtonInstance)
		$ScrollContainer/GameButtons.move_child(PublicGameButtonInstance, 1)

func _on_PublicGamesMenuPanel_visibility_changed() -> void:
	if visible:
		# Get public games
		Network.rpc_id(1, "get_public_games", multiplayer.get_unique_id(), Settings.User)

func _on_RefreshGamesButton_pressed() -> void:
	# Refresh public games
	Network.rpc_id(1, "get_public_games", multiplayer.get_unique_id(), Settings.User)
