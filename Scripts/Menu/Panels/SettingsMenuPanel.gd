extends "res://Scripts/Menu/Panels/MenuPanel.gd"

@export var ConnectToServerTime : int = 3

# Returns true if connection settings are valid
func get_connection_settings_valid() -> bool:
	if Settings.User == "":
		# Show input user popup
		get_node("/root/Menu/%InputUserPopup").show()
		return false
	elif Settings.ServerIp == "":
		# Show input server ip popup
		get_node("/root/Menu/%InputServerIpPopup").show()
		return false
	elif Settings.ServerPort == 0:
		# Show input server port popup
		get_node("/root/Menu/%InputServerPortPopup").show()
		return false
	return true

func stop_connection_timer() -> void:
	$ConnectionTimer.disconnect("timeout", _on_ConnectionTimer_timeout)
	$ConnectionTimer.stop()
	$ConnectionTimer.connect("timeout", _on_ConnectionTimer_timeout)

func _on_SettingsMenuPanel_visibility_changed() -> void:
	if visible:
		# Set starting inputs
		$"%User/LineEdit".text = str(Settings.User)
		_on_UserLineEdit_text_changed($"%User/LineEdit".text)
		$"%ServerIp/LineEdit".text = str(Settings.ServerIp)
		_on_ServerIpLineEdit_text_changed($"%ServerIp/LineEdit".text)
		if Settings.ServerPort != 0:
			$"%ServerPort/LineEdit".text = str(Settings.ServerPort)
		else:
			$"%ServerPort/LineEdit".text = ""
		_on_ServerPortLineEdit_text_changed($"%ServerPort/LineEdit".text)

func _on_UserLineEdit_text_changed(newtext : String) -> void:
	$"%User/ErrorToolTipButton".visible = newtext == ""

func _on_ServerIpLineEdit_text_changed(newtext : String) -> void:
	$"%ServerIp/ErrorToolTipButton".visible = newtext == ""

func _on_ServerPortLineEdit_text_changed(newtext : String) -> void:
	$"%ServerPort/ErrorToolTipButton".visible = newtext == ""

func _on_ConnectToServerButton_pressed() -> void:
	var User : String = $"%User/LineEdit".text
	var ServerIp : String = $"%ServerIp/LineEdit".text
	var ServerPort : String = $"%ServerPort/LineEdit".text
	# Sets settings variables
	Settings.User = User
	Settings.ServerIp = ServerIp
	Settings.ServerPort = int(ServerPort)
	# Checks if connection settings are valid
	if get_node("/root/Menu").get_connection_settings_valid():
		# TODO: move
		# Writes setting variables to config file
		get_node("/root/Menu").Config.set_value("Account", "User", Settings.User)
		get_node("/root/Menu").Config.set_value("Connection", "ServerIp", Settings.ServerIp)
		get_node("/root/Menu").Config.set_value("Connection", "ServerPort", Settings.ServerPort)
		get_node("/root/Menu").Config.save(get_node("/root/Menu").get_config_path())
		# Starts connecting to server
		Network.connect_to_server()
		$ConnectionTimer.start(ConnectToServerTime)
		# Show connecting popup
		get_node("/root/Menu/%ConnectingPopup").show()

func _on_ConnectionTimer_timeout() -> void:
	Network.close_server_connection()

func _on_BackButton_pressed() -> void:
	if multiplayer.get_peers().is_empty():
		# Show select server popup
		get_node("/root/Menu/%SelectServerPopup").show()
	else:
		# Show back menu panel
		get_node("/root/Menu/%" + BackMenu).show()
