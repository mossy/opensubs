extends "res://Scripts/InterfaceElements/Popup.gd"

func _on_DefaultServerButton_pressed() -> void:
	Settings.ServerIp = "5.161.129.232"
	Settings.ServerPort = 25565
	# TODO: remove
	hide()
	# Show input user popup
	get_node("/root/Menu/%InputUserPopup").show()

func _on_CustomServerButton_pressed() -> void:
	hide()
	# Show settings menu panel
	get_node("/root/Menu/%SettingsMenuPanel").show()
