extends Node2D

var Order : Dictionary

var InvalidReasons : Array
var WarningReasons : Array

var ItemNode : Node
var ItemHistory : Array

var CreateItemOrderNode : Node 
var CreateItemOrder : Dictionary

# Inbuilt functions

func _ready() -> void:
	# Set nodes variables
	set_nodes()
	# Set nodes active
	set_item_node_active()
	# Calculate game state expansion
	if GameVariables.AllActive == false:
		get_node("/root/Game").calculate_game_state_expansion(Order["OrderTick"])
	# Set queued order
	if Order["OrderTick"] > GameVariables.GlobalTick:
		get_node("/root/Game/%TimePanel").set_queued_order(self)

# Set order functions

func calculate_effect(tick : int) -> void:
	return

# Node functions

func set_nodes() -> void:
	# Get create item order variables
	CreateItemOrderNode = get_create_order_node()
	CreateItemOrder = CreateItemOrderNode.Order
	# Get item variables
	ItemNode = get_item_node()
	ItemHistory = ItemNode.History
	# Set order in item orders
	if ItemNode.ItemOrderNodes.has(Order["OrderTick"]) == false:
		ItemNode.ItemOrderNodes[Order["OrderTick"]] = []
	ItemNode.ItemOrderNodes[Order["OrderTick"]].append(self)

func get_create_order_node() -> Node:
	return Node.new()

func get_item_node() -> Node:
	return Node.new()

func set_item_node_active() -> void:
	ItemNode.set_active(Order["OrderTick"])

func remove_order(calculategamestate : bool = true) -> void:
	queue_free()
	# Remove order from item orders
	ItemNode.ItemOrderNodes[Order["OrderTick"]].erase(self)
	if ItemNode.ItemOrderNodes[Order["OrderTick"]].is_empty():
		ItemNode.ItemOrderNodes.erase(Order["OrderTick"])
	# Set nodes active
	set_item_node_active()
	# Set order invalid reasons canceled
	InvalidReasons = ["Canceled"]
	# Remove order from server
	Network.rpc_id(
		1,
		"remove_server_order",
		# Game ID
		GameVariables.GameId,
		# Client ID
		multiplayer.get_unique_id(),
		# Order tick
		Order["OrderTick"],
		# Order ID
		Order["OrderId"]
	)
	# Check order has related order id reference
	var RelatedOrdersIdReference : String = get_related_orders_id_reference()
	if RelatedOrdersIdReference != "":
		# Remove related orders
		for relatedorderticknode in get_node("/root/Game/Orders/Submarines").get_children():
			if int(str(relatedorderticknode.name)) >= Order["OrderTick"]:
				for relatedordernode in relatedorderticknode.get_children():
					var RelatedOrder : Dictionary = relatedordernode.Order
					if RelatedOrder.get(RelatedOrdersIdReference, -1) == Order["OrderId"]:
						relatedordernode.remove_order(false)
	# Calculate game state
	if calculategamestate:
		get_node("/root/Game").calculate_game_state_expansion(Order["OrderTick"])

func get_related_orders_id_reference() -> String:
	return ""

# Order reasons functions

func set_invalid_reasons(tick : int) -> void:
	return

func set_warning_reasons(tick : int) -> void:
	return

func get_combined_reasons() -> Array:
	var CombinedReasons : Array
	if get("InvalidReasons") != null:
		CombinedReasons += get("InvalidReasons")
	if get("WarningReasons") != null:
		CombinedReasons += get("WarningReasons")
	return CombinedReasons

# Time functions

# Returns when order starts
func get_order_time() -> int:
	var StartTime : int = GameVariables.StartTime
	if StartTime == -1:
		StartTime = Time.get_unix_time_from_system()
	var OrderTime : int = max(
		StartTime + (Order["OrderTick"] * GameVariables.TickLength),
		Order["Time"]
	)
	return OrderTime

# Return the time until an order starts
func get_time_to_order() -> int:
	var OrderTime : int = get_order_time()
	return OrderTime - Time.get_unix_time_from_system() + 1

func get_ticks_to_order(tick : int) -> int:
	var OrderTick : int = Order["OrderTick"]
	return OrderTick - tick

# Returns the time until an goes into effect
func get_time_to_order_effect(tick : int) -> int:
	var TimeToOrder : int
	if tick == GameVariables.GlobalTick:
		TimeToOrder = get_time_to_order()
	else:
		TimeToOrder = get_ticks_to_order(tick) * GameVariables.TickLength
	return TimeToOrder + (GameVariables.OrderWaitTicks * GameVariables.TickLength)

# Returns true if the order has been started but has yet to go into effect
func get_awaiting_order_effect(tick : int) -> bool:
	var TimeToOrderEffect : int = get_time_to_order_effect(tick)
	return (
		TimeToOrderEffect > 0 and
		TimeToOrderEffect <= GameVariables.OrderWaitTicks * GameVariables.TickLength and
		InvalidReasons.has("Canceled") == false
	)
