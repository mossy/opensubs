extends "res://Scripts/Orders/Outposts/OutpostOrder.gd"

var OutpostScene : PackedScene = preload("res://Scenes/Items/Outposts/Outpost.tscn")

# Node functions

func get_create_order_node() -> Node:
	return self

func get_item_node() -> Node:
	# Get outpost instance
	ItemNode = OutpostScene.instantiate()
	# Set outpost name
	var OutpostNamesList : Array = get_node("/root/Game/Orders/Outposts").OutpostNamesList
	var OutpostName = OutpostNamesList[randi_range(0, len(OutpostNamesList) - 1)]
	OutpostNamesList.erase(OutpostName)
	ItemNode.name = OutpostName
	# Set outpost create item order node
	ItemNode.CreateItemOrderNode = self
	# Add outpost as outposts child
	get_node("/root/Game/Items/Outposts").add_child(ItemNode)
	# Return outpost node
	return ItemNode
