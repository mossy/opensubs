extends "res://Scripts/Orders/Order.gd"

# Node functions

func set_nodes() -> void:
	super()
	# Set order variables
	Order["Outpost"] = ItemNode.name
	# Connect create outpost order queue free to self queue free
	CreateItemOrderNode.connect("tree_exiting", queue_free)

func get_create_order_node() -> Node:
	return get_node("/root/Game/Orders/Outposts/" + str(Order["CreateOutpostOrderTick"]) + "/" + str(Order["CreateOutpostOrderId"]))

func get_item_node() -> Node:
	return get_node("/root/Game/Items/Outposts/" + CreateItemOrder["Outpost"])
