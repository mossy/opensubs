extends "res://Scripts/Orders/Outposts/OutpostOrder.gd"

func calculate_effect(tick : int) -> void:
	if InvalidReasons.is_empty():
		if get_time_to_order_effect(tick) == 0:
			print("Caculating order " + str(Order["OrderId"]) + " effect at " + str(tick))
			ItemNode.set_history_type(tick, Order["TransformType"])
			ItemNode.set_history_troops(tick, -GameVariables.MineTroopsCost)

# Reasons functions

func set_invalid_reasons(tick : int) -> void:
	# Check that submarine is not canceled
	if InvalidReasons.has("Canceled") == false:
		# Check if tick is order tick
		if tick == Order["OrderTick"]:
			# Reset invalid reasons
			InvalidReasons = []
		# Check that order is awaiting effect
		if get_awaiting_order_effect(tick):
			# Get data
			var HistoryEntry : Dictionary = ItemNode.get_history_entry(tick)
			# Check if order requires too many troops
			if GameVariables.MineTroopsCost > HistoryEntry["TroopsTotal"]:
				InvalidReasons.append("TooManyTroops")
			# Check if the outpost has been captured
			if HistoryEntry["CurrentPlayer"] != get_node("/root/Game").get_user_player(Order["User"]):
				InvalidReasons.append("OutpostCaptured")
