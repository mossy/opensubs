extends "res://Scripts/Orders/Players/PlayerOrder.gd"

var PlayerScene : PackedScene = preload("res://Scenes/Items/Players/Player.tscn")

# Node functions

func get_create_order_node() -> Node:
	return self

func get_item_node() -> Node:
	# Get player instance
	ItemNode = PlayerScene.instantiate()
	# Set player name
	var PlayersNameList : Array = get_node("/root/Game/Orders/Players").PlayerNamesList
	var PlayerName = PlayersNameList[randi_range(0, len(PlayersNameList) - 1)]
	PlayersNameList.erase(PlayerName)
	ItemNode.name = PlayerName
	# Set player create item order node
	ItemNode.CreateItemOrderNode = self
	# Add player as players child
	get_node("/root/Game/Items/Players").add_child(ItemNode)
	# Return player node
	return ItemNode
