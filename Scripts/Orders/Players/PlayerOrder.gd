extends "res://Scripts/Orders/Order.gd"

# Node functions

func set_nodes() -> void:
	super()
	# Set order variables
	Order["Player"] = ItemNode.name
	# Set game variables
	if Settings.User == Order["User"]:
		GameVariables.Player = Order["Player"]
	# Connect create player order queue free to self queue free
	CreateItemOrderNode.connect("tree_exiting", queue_free)

func get_create_order_node() -> Node:
	return get_node("/root/Game/Orders/Players/" + str(Order["CreatePlayerOrderTick"]) + "/" + str(Order["CreatePlayerOrderId"]))

func get_item_node() -> Node:
	return get_node("/root/Game/Items/Players/" + CreateItemOrder["Player"])

# Set order functions

func set_user() -> void:
	if Order["Type"] == "CreatePlayer":
		if get_index() < len(GameVariables.Users):
			Order["User"] = GameVariables.Users[get_index()]
