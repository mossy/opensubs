extends "res://Scripts/Orders/Submarines/SubmarineOrder.gd"

var SubmarineScene : PackedScene = preload("res://Scenes/Items/Submarines/Submarine.tscn")

# Node functions

func get_create_order_node() -> Node:
	return self

func get_item_node() -> Node:
	# Get submarine instance
	ItemNode = SubmarineScene.instantiate()
	# Set submarine name
	ItemNode.name = Order["InitialOutpost"] + "-" + str(Order["OrderId"])
	# Set submarine create item order node
	ItemNode.CreateItemOrderNode = self
	# Add submarine node as submarines child
	get_node("/root/Game/Items/Submarines").add_child(ItemNode)
	# Return submarine node
	return ItemNode

func get_related_orders_id_reference() -> String:
	return "CreateSubmarineOrderId"

# Set order functions

func set_initial_troops(troops : int) -> void:
	# Set troops
	Order["InitialTroops"] = troops
	# Set nodes active
	set_item_node_active()
	# Calculate game state expansion
	get_node("/root/Game").calculate_game_state_expansion(Order["OrderTick"])
	# Update submarine
	get_node("/root/Game/Items/Submarines/" + Order["Submarine"]).override_temporary_troops()
	# Update initial outpost
	get_node("/root/Game/Items/Outposts/" + Order["InitialOutpost"]).override_temporary_troops()

# Reasons functions

func set_invalid_reasons(tick : int) -> void:
	# Check that order has not been canceled
	if InvalidReasons.has("Canceled") == false:
		# Check if tick is order tick
		if tick == Order["OrderTick"]:
			# Reset invalid reasons
			InvalidReasons = []
			# Get data
			var HistoryEntry : Dictionary = ItemNode.get_history_entry(tick)
			var InitialOutpostHistoryEntry : Dictionary = InitialOutpostNode.get_history_entry(tick)
			# Get available troops
			var InitialOutpostAvailableTroops : int = InitialOutpostHistoryEntry["TroopsTotal"]
			if InitialOutpostNode.Active == false:
				InitialOutpostAvailableTroops += HistoryEntry["TroopsTotal"]
			# Check if order requires too few troops
			if Order["InitialTroops"] <= 0:
				InvalidReasons.append("TooFewTroops")
			# Check if order requires too many troops
			elif Order["InitialTroops"] > InitialOutpostAvailableTroops:
				InvalidReasons.append("TooManyTroops")
			# Check if outpost has been captured
			if InitialOutpostHistoryEntry["CurrentPlayer"] != get_node("/root/Game").get_user_player(Order["User"]):
				InvalidReasons.append("OutpostCaptured")
