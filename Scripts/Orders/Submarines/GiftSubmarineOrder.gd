extends "res://Scripts/Orders/Submarines/SubmarineOrder.gd"

# Set order functions

func calculate_effect(tick : int) -> void:
	# TODO: reconsider
	# Set submarine gift
	if InvalidReasons.is_empty():
		if get_time_to_order_effect(tick) == 0:
			print("Caculating order " + str(Order["OrderId"]) + " effect at " + str(tick))
			ItemNode.set_history_type(tick, "Gift")

# Reasons functions

func set_invalid_reasons(tick : int) -> void:
	# Check that order has not been canceled
	if InvalidReasons.has("Canceled") == false:
		# Check if tick is order tick
		if tick == Order["OrderTick"]:
			# Reset invalid reasons
			InvalidReasons = []
		# Check that order is awaiting effect
		if get_awaiting_order_effect(tick):
			# Get item data
			var ItemPreviousHistoryEntry : Dictionary = ItemNode.get_previous_history_entry(tick)
			# Checks if the gift order is set before submarine launches
			if tick < ItemHistory[0]["Tick"]:
				InvalidReasons.append("GiftBeforeLaunch")
			# Checks if the gift order is set after submarine arrives
			elif tick > ItemHistory[-1]["Tick"]:
				InvalidReasons.append("GiftAfterArrival")
			# Checks if the submarine is invalid
			if CreateItemOrderNode.InvalidReasons.is_empty() == false:
				InvalidReasons.append("InvalidCreateSubmarineOrder")
			# Checks if the submarine has already gifted
			if ItemPreviousHistoryEntry["CurrentType"] == "Gift":
				InvalidReasons.append("GiftAlready")

func set_warning_reasons(tick : int) -> void:
	# Check that order has not been canceled
	if InvalidReasons.has("Canceled") == false:
		# Check if tick is order tick
		if tick == Order["OrderTick"]:
			# Reset warning reasons
			WarningReasons = []
			# Set reasons assumed true
			WarningReasons.append("GiftEffectAfterArrival")
		# Check if tick is effect tick
		elif tick == Order["OrderTick"] + GameVariables.OrderWaitTicks:
			# Remove reasons assumed true
			WarningReasons.erase("GiftEffectAfterArrival")
			# Set reasons proved true
			if ItemNode.get_history_entry(tick)["CurrentPlayer"] != CreateItemOrder["InitialPlayer"]:
				WarningReasons.append("GiftEffectAfterCapture")
