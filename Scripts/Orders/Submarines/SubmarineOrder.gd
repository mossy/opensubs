extends "res://Scripts/Orders/Order.gd"

# Initial outpost variables

var InitialOutpostNode : Node
var InitialOutpostHistory : Array

# Node functions

func set_nodes() -> void:
	super()
	# Set initial outpost variables
	InitialOutpostNode = ItemNode.InitialOutpostNode
	InitialOutpostHistory = InitialOutpostNode.History
	# Set order variables
	Order["Submarine"] = ItemNode.name
	# Connect create submarine order queue free to self queue free
	CreateItemOrderNode.connect("tree_exiting", queue_free)

func get_create_order_node() -> Node:
	return get_node("/root/Game/Orders/Submarines/" + str(Order["CreateSubmarineOrderTick"]) + "/" + str(Order["CreateSubmarineOrderId"]))

func get_item_node() -> Node:
	return get_node("/root/Game/Items/Submarines/" + CreateItemOrder["Submarine"])
