extends Node

var Colors : Dictionary = {
	"Dormant" : "4C4C4C",
	"Red" : "AB2024",
	"Orange" : "D28E2A",
	"Cyan" : "5C9A8C",
	"Sky" : "7BA7D8",
	"Blue" : "3A4BA3",
	"Purple" : "6A558E",
	"Pink" : "C777B1",
	"Olive" : "686E47",
	"Beige" : "9B907B",
	"Brown" : "8B5E3B"
}

# Default game setup variables

var DefaultGenerateTroopsTicks : int = 50
var DefaultGenerateShieldMaximumTicks : int = 300
var DefaultGenerateOreTicks : int = 150
var DefaultSonarRange : float = 400.0
var DefaultSubmarineSpeed : float = 5.0
var DefaultChargePerGenerator : int = 120
var TickLengthsList : Array = [1, 5, 10, 30, 60, 300, 600]
var VictoryTypesList : Array = ["Domination", "Mining"]
var MapTypesList : Array = ["RandomSpread", "Grid"]
var OutpostNamesList : Array = ["Classics", "Capitals"]

# Game setup variables

# Basic setup variables
var TickLength : int
# Victory setup variables
var VictoryType : String
var DominationVictoryPercentage : float
var MiningVictoryTotal : int
# Map setup variables
var MapType : String
var OutpostsPerPlayer : int
var OutpostNames : String
# Multiplier setup variables
var GenerateShieldMaximumTicksDivisor : float
var GenerateTroopsTicksDivisor : float
var GenerateOreTicksDivisor : float
var SonarRangeMultiplier : float
var SubmarineSpeedMultiplier : int
# Advanced setup variables
var OrderWaitTicks : int
# Unimplemented setup variables
var InitialOutpostTroops : int
var InitialOutpostShield : int
var OutpostSpacing : int
var GenerateTroopsNumber : int
var MineTroopsCost : int

# Game state variables

# Game variables
var GameName : String
var GameId : int
# TODO: remove
var Orders : Dictionary
# Time variables
var StartTime : int = -1
# Tick variables
# TODO: remove
var AllActive : bool = false
var GlobalTick : int
var VisibleTick : int
var MaximumTick : int
var Jumping : bool
# Generation variables
var GenerateShieldMaximumTicks : int
var GenerateTroopsTicks : int
var GenerateOreTicks : int
# Player variables
var PlayerCount : int
var Player : String
var Users : Array
var OnlineUsers : Dictionary
var Chats : Dictionary
# Map variables
var MapSize : Vector2
var HalfMapSize : Vector2
var OutpostsTotal : int
# Game end variables
var GameEndTick : int = -1
var GameEndUser : String
var DominationVictoryTotal : int
