extends Node

# User settings

var User : String
var ServerIp : String
var ServerPort : int
var CalculateTroopsDelay : int = 0.2
var MaximumTickAhead : int = 300

# Hidden settings

var ConnectionTimeoutLength : int = 3
