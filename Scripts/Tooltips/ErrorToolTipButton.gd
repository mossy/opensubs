extends "res://Scripts/Tooltips/TooltipButton.gd"

func _on_toggled(buttonpressed : bool) -> void:
	get_node("../ErrorToolTip").visible = buttonpressed
