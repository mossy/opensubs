extends Button

func _on_mouse_entered() -> void:
	button_pressed = true

func _on_mouse_exited() -> void:
	button_pressed = false
